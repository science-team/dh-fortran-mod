* Automate  library name mangling ?
* Some suppport for cmake, meson ?

Plan:

TARGET 0.40: 
 * Realistic autopkgtests
 * build-chain specific compiler flags

TARGET 0.50:
 * tests, including submodule support
 * Handle testing multiple versions of same compiler.
   - eg flang20, with flang-19 as default?
 * Multiple default compilers ?


TARGET 1.0:
 * The Great renaming: dh_fortran_mod -> dh_fortran 


Elsewhere:
 * Patches for gfortran, flang, lfortran to add Debian FMODDIR to search path
 * libtool, configure support ?
 * cmake support ?
