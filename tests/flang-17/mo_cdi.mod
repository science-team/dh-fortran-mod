﻿!mod$ v1 sum:7e2ab774f8aafeb4
module mo_cdi
use,intrinsic::iso_c_binding,only:c_f_pointer
use,intrinsic::iso_c_binding,only:c_ptr
use,intrinsic::iso_c_binding,only:c_funptr
use,intrinsic::iso_c_binding,only:c_sizeof
use,intrinsic::iso_c_binding,only:c_loc
use,intrinsic::iso_c_binding,only:operator(==)
use,intrinsic::iso_c_binding,only:operator(/=)
use,intrinsic::iso_c_binding,only:c_null_ptr
use,intrinsic::iso_c_binding,only:c_null_funptr
use,intrinsic::iso_c_binding,only:c_int8_t
use,intrinsic::iso_c_binding,only:c_int16_t
use,intrinsic::iso_c_binding,only:c_int32_t
use,intrinsic::iso_c_binding,only:c_int64_t
use,intrinsic::iso_c_binding,only:c_int128_t
use,intrinsic::iso_c_binding,only:c_int
use,intrinsic::iso_c_binding,only:c_short
use,intrinsic::iso_c_binding,only:c_long
use,intrinsic::iso_c_binding,only:c_long_long
use,intrinsic::iso_c_binding,only:c_signed_char
use,intrinsic::iso_c_binding,only:c_size_t
use,intrinsic::iso_c_binding,only:c_intmax_t
use,intrinsic::iso_c_binding,only:c_intptr_t
use,intrinsic::iso_c_binding,only:c_ptrdiff_t
use,intrinsic::iso_c_binding,only:c_int_least8_t
use,intrinsic::iso_c_binding,only:c_int_fast8_t
use,intrinsic::iso_c_binding,only:c_int_least16_t
use,intrinsic::iso_c_binding,only:c_int_fast16_t
use,intrinsic::iso_c_binding,only:c_int_least32_t
use,intrinsic::iso_c_binding,only:c_int_fast32_t
use,intrinsic::iso_c_binding,only:c_int_least64_t
use,intrinsic::iso_c_binding,only:c_int_fast64_t
use,intrinsic::iso_c_binding,only:c_int_least128_t
use,intrinsic::iso_c_binding,only:c_int_fast128_t
use,intrinsic::iso_c_binding,only:c_float
use,intrinsic::iso_c_binding,only:c_double
use,intrinsic::iso_c_binding,only:c_long_double
use,intrinsic::iso_c_binding,only:c_float_complex
use,intrinsic::iso_c_binding,only:c_double_complex
use,intrinsic::iso_c_binding,only:c_long_double_complex
use,intrinsic::iso_c_binding,only:c_bool
use,intrinsic::iso_c_binding,only:c_char
use,intrinsic::iso_c_binding,only:c_null_char
use,intrinsic::iso_c_binding,only:c_alert
use,intrinsic::iso_c_binding,only:c_backspace
use,intrinsic::iso_c_binding,only:c_form_feed
use,intrinsic::iso_c_binding,only:c_new_line
use,intrinsic::iso_c_binding,only:c_carriage_return
use,intrinsic::iso_c_binding,only:c_horizontal_tab
use,intrinsic::iso_c_binding,only:c_vertical_tab
use,intrinsic::iso_c_binding,only:c_associated
use,intrinsic::iso_c_binding,only:c_f_procpointer
use,intrinsic::iso_c_binding,only:c_float128
use,intrinsic::iso_c_binding,only:c_float128_complex
use,intrinsic::iso_c_binding,only:iso_c_binding$iso_c_binding$c_associated_c_ptr=>c_associated_c_ptr
use,intrinsic::iso_c_binding,only:c_funloc
private::c_f_pointer
private::c_ptr
private::c_funptr
private::c_sizeof
private::c_loc
private::operator(==)
private::operator(/=)
private::c_null_ptr
private::c_null_funptr
private::c_int8_t
private::c_int16_t
private::c_int32_t
private::c_int64_t
private::c_int128_t
private::c_int
private::c_short
private::c_long
private::c_long_long
private::c_signed_char
private::c_size_t
private::c_intmax_t
private::c_intptr_t
private::c_ptrdiff_t
private::c_int_least8_t
private::c_int_fast8_t
private::c_int_least16_t
private::c_int_fast16_t
private::c_int_least32_t
private::c_int_fast32_t
private::c_int_least64_t
private::c_int_fast64_t
private::c_int_least128_t
private::c_int_fast128_t
private::c_float
private::c_double
private::c_long_double
private::c_float_complex
private::c_double_complex
private::c_long_double_complex
private::c_bool
private::c_char
private::c_null_char
private::c_alert
private::c_backspace
private::c_form_feed
private::c_new_line
private::c_carriage_return
private::c_horizontal_tab
private::c_vertical_tab
private::c_associated
private::c_f_procpointer
private::c_float128
private::c_float128_complex
private::iso_c_binding$iso_c_binding$c_associated_c_ptr
private::c_funloc
private::lib_strlen
interface
function lib_strlen(charptr) bind(c,name="strlen")
import::c_ptr
type(c_ptr),value::charptr
integer(8)::lib_strlen
end
end interface
private::lib_free
interface
subroutine lib_free(ptr) bind(c,name="free")
import::c_ptr
type(c_ptr),intent(in),value::ptr
end
end interface
integer(4),parameter::cdi_max_name=256_4
integer(4),parameter::cdi_undefid=-1_4
integer(4),parameter::cdi_global=-1_4
integer(4),parameter::cdi_xaxis=1_4
integer(4),parameter::cdi_yaxis=2_4
integer(4),parameter::cdi_bigendian=0_4
integer(4),parameter::cdi_littleendian=1_4
integer(4),parameter::cdi_pdpendian=2_4
integer(4),parameter::cdi_real=1_4
integer(4),parameter::cdi_comp=2_4
integer(4),parameter::cdi_both=3_4
integer(4),parameter::cdi_noerr=0_4
integer(4),parameter::cdi_eeof=-1_4
integer(4),parameter::cdi_etmof=-9_4
integer(4),parameter::cdi_esystem=-10_4
integer(4),parameter::cdi_einval=-20_4
integer(4),parameter::cdi_eisdir=-21_4
integer(4),parameter::cdi_eisempty=-22_4
integer(4),parameter::cdi_euftype=-23_4
integer(4),parameter::cdi_elibnavail=-24_4
integer(4),parameter::cdi_eufstruct=-25_4
integer(4),parameter::cdi_eunc4=-26_4
integer(4),parameter::cdi_edimsize=-27_4
integer(4),parameter::cdi_eqenf=-50_4
integer(4),parameter::cdi_eqnavail=-51_4
integer(4),parameter::cdi_elimit=-99_4
integer(4),parameter::cdi_filetype_grb=1_4
integer(4),parameter::cdi_filetype_grb2=2_4
integer(4),parameter::cdi_filetype_nc=3_4
integer(4),parameter::cdi_filetype_nc2=4_4
integer(4),parameter::cdi_filetype_nc4=5_4
integer(4),parameter::cdi_filetype_nc4c=6_4
integer(4),parameter::cdi_filetype_nc5=7_4
integer(4),parameter::cdi_filetype_srv=8_4
integer(4),parameter::cdi_filetype_ext=9_4
integer(4),parameter::cdi_filetype_ieg=10_4
integer(4),parameter::cdi_filetype_nczarr=11_4
integer(4),parameter::filetype_grb=1_4
integer(4),parameter::filetype_grb2=2_4
integer(4),parameter::filetype_nc=3_4
integer(4),parameter::filetype_nc2=4_4
integer(4),parameter::filetype_nc4=5_4
integer(4),parameter::cdi_protocol_other=0_4
integer(4),parameter::cdi_protocol_file=1_4
integer(4),parameter::cdi_protocol_fdb=2_4
integer(4),parameter::cdi_protocol_across=3_4
integer(4),parameter::cdi_compress_none=0_4
integer(4),parameter::cdi_compress_szip=1_4
integer(4),parameter::cdi_compress_aec=2_4
integer(4),parameter::cdi_compress_zip=3_4
integer(4),parameter::cdi_compress_jpeg=4_4
integer(4),parameter::cdi_compress_filter=5_4
integer(4),parameter::datatype_pack16=16_4
integer(4),parameter::datatype_pack24=24_4
integer(4),parameter::datatype_flt32=132_4
integer(4),parameter::datatype_flt64=164_4
integer(4),parameter::datatype_int32=232_4
integer(4),parameter::datatype_int=251_4
integer(4),parameter::cdi_datatype_pack=0_4
integer(4),parameter::cdi_datatype_pack1=1_4
integer(4),parameter::cdi_datatype_pack2=2_4
integer(4),parameter::cdi_datatype_pack3=3_4
integer(4),parameter::cdi_datatype_pack4=4_4
integer(4),parameter::cdi_datatype_pack5=5_4
integer(4),parameter::cdi_datatype_pack6=6_4
integer(4),parameter::cdi_datatype_pack7=7_4
integer(4),parameter::cdi_datatype_pack8=8_4
integer(4),parameter::cdi_datatype_pack9=9_4
integer(4),parameter::cdi_datatype_pack10=10_4
integer(4),parameter::cdi_datatype_pack11=11_4
integer(4),parameter::cdi_datatype_pack12=12_4
integer(4),parameter::cdi_datatype_pack13=13_4
integer(4),parameter::cdi_datatype_pack14=14_4
integer(4),parameter::cdi_datatype_pack15=15_4
integer(4),parameter::cdi_datatype_pack16=16_4
integer(4),parameter::cdi_datatype_pack17=17_4
integer(4),parameter::cdi_datatype_pack18=18_4
integer(4),parameter::cdi_datatype_pack19=19_4
integer(4),parameter::cdi_datatype_pack20=20_4
integer(4),parameter::cdi_datatype_pack21=21_4
integer(4),parameter::cdi_datatype_pack22=22_4
integer(4),parameter::cdi_datatype_pack23=23_4
integer(4),parameter::cdi_datatype_pack24=24_4
integer(4),parameter::cdi_datatype_pack25=25_4
integer(4),parameter::cdi_datatype_pack26=26_4
integer(4),parameter::cdi_datatype_pack27=27_4
integer(4),parameter::cdi_datatype_pack28=28_4
integer(4),parameter::cdi_datatype_pack29=29_4
integer(4),parameter::cdi_datatype_pack30=30_4
integer(4),parameter::cdi_datatype_pack31=31_4
integer(4),parameter::cdi_datatype_pack32=32_4
integer(4),parameter::cdi_datatype_cpx32=64_4
integer(4),parameter::cdi_datatype_cpx64=128_4
integer(4),parameter::cdi_datatype_flt32=132_4
integer(4),parameter::cdi_datatype_flt64=164_4
integer(4),parameter::cdi_datatype_int8=208_4
integer(4),parameter::cdi_datatype_int16=216_4
integer(4),parameter::cdi_datatype_int32=232_4
integer(4),parameter::cdi_datatype_uint8=308_4
integer(4),parameter::cdi_datatype_uint16=316_4
integer(4),parameter::cdi_datatype_uint32=332_4
integer(4),parameter::cdi_datatype_int=251_4
integer(4),parameter::cdi_datatype_flt=252_4
integer(4),parameter::cdi_datatype_txt=253_4
integer(4),parameter::cdi_datatype_cpx=254_4
integer(4),parameter::cdi_datatype_uchar=255_4
integer(4),parameter::cdi_datatype_long=256_4
integer(4),parameter::cdi_datatype_uint=257_4
integer(4),parameter::cdi_chunk_auto=1_4
integer(4),parameter::cdi_chunk_grid=2_4
integer(4),parameter::cdi_chunk_lines=3_4
integer(4),parameter::grid_generic=1_4
integer(4),parameter::grid_gaussian=2_4
integer(4),parameter::grid_gaussian_reduced=3_4
integer(4),parameter::grid_lonlat=4_4
integer(4),parameter::grid_spectral=5_4
integer(4),parameter::grid_fourier=6_4
integer(4),parameter::grid_gme=7_4
integer(4),parameter::grid_trajectory=8_4
integer(4),parameter::grid_unstructured=9_4
integer(4),parameter::grid_curvilinear=10_4
integer(4),parameter::grid_projection=12_4
integer(4),parameter::grid_charxy=13_4
integer(4),parameter::cdi_proj_rll=21_4
integer(4),parameter::cdi_proj_lcc=22_4
integer(4),parameter::cdi_proj_laea=23_4
integer(4),parameter::cdi_proj_sinu=24_4
integer(4),parameter::cdi_proj_stere=25_4
integer(4),parameter::cdi_proj_healpix=26_4
integer(4),parameter::zaxis_surface=0_4
integer(4),parameter::zaxis_generic=1_4
integer(4),parameter::zaxis_hybrid=2_4
integer(4),parameter::zaxis_hybrid_half=3_4
integer(4),parameter::zaxis_pressure=4_4
integer(4),parameter::zaxis_height=5_4
integer(4),parameter::zaxis_depth_below_sea=6_4
integer(4),parameter::zaxis_depth_below_land=7_4
integer(4),parameter::zaxis_isentropic=8_4
integer(4),parameter::zaxis_trajectory=9_4
integer(4),parameter::zaxis_altitude=10_4
integer(4),parameter::zaxis_sigma=11_4
integer(4),parameter::zaxis_meansea=12_4
integer(4),parameter::zaxis_toa=13_4
integer(4),parameter::zaxis_sea_bottom=14_4
integer(4),parameter::zaxis_atmosphere=15_4
integer(4),parameter::zaxis_cloud_base=16_4
integer(4),parameter::zaxis_cloud_top=17_4
integer(4),parameter::zaxis_isotherm_zero=18_4
integer(4),parameter::zaxis_snow=19_4
integer(4),parameter::zaxis_lake_bottom=20_4
integer(4),parameter::zaxis_sediment_bottom=21_4
integer(4),parameter::zaxis_sediment_bottom_ta=22_4
integer(4),parameter::zaxis_sediment_bottom_tw=23_4
integer(4),parameter::zaxis_mix_layer=24_4
integer(4),parameter::zaxis_reference=25_4
integer(4),parameter::zaxis_char=26_4
integer(4),parameter::zaxis_tropopause=27_4
integer(4),parameter::max_kv_pairs_match=10_4
integer(4),parameter::time_constant=0_4
integer(4),parameter::time_varying=1_4
integer(4),parameter::time_variable=1_4
integer(4),parameter::tstep_constant=0_4
integer(4),parameter::tstep_instant=1_4
integer(4),parameter::tstep_avg=2_4
integer(4),parameter::tstep_accum=3_4
integer(4),parameter::tstep_max=4_4
integer(4),parameter::tstep_min=5_4
integer(4),parameter::tstep_diff=6_4
integer(4),parameter::tstep_rms=7_4
integer(4),parameter::tstep_sd=8_4
integer(4),parameter::tstep_cov=9_4
integer(4),parameter::tstep_ratio=10_4
integer(4),parameter::tstep_sum=11_4
integer(4),parameter::tstep_range=12_4
integer(4),parameter::tstep_instant2=13_4
integer(4),parameter::tstep_instant3=14_4
integer(4),parameter::taxis_absolute=1_4
integer(4),parameter::taxis_relative=2_4
integer(4),parameter::taxis_forecast=3_4
integer(4),parameter::tunit_second=1_4
integer(4),parameter::tunit_minute=2_4
integer(4),parameter::tunit_quarter=3_4
integer(4),parameter::tunit_30minutes=4_4
integer(4),parameter::tunit_hour=5_4
integer(4),parameter::tunit_3hours=6_4
integer(4),parameter::tunit_6hours=7_4
integer(4),parameter::tunit_12hours=8_4
integer(4),parameter::tunit_day=9_4
integer(4),parameter::tunit_month=10_4
integer(4),parameter::tunit_year=11_4
integer(4),parameter::calendar_standard=0_4
integer(4),parameter::calendar_gregorian=1_4
integer(4),parameter::calendar_proleptic=2_4
integer(4),parameter::calendar_360days=3_4
integer(4),parameter::calendar_365days=4_4
integer(4),parameter::calendar_366days=5_4
integer(4),parameter::calendar_none=6_4
integer(4),parameter::cdi_uuid_size=16_4
type,bind(c)::t_cdiparam
integer(4)::discipline
integer(4)::category
integer(4)::number
end type
type::t_cdiiterator
type(c_ptr)::ptr
end type
type::t_cdigribiterator
type(c_ptr)::ptr
end type
integer(4),parameter::cdi_key_name=942_4
integer(4),parameter::cdi_key_longname=943_4
integer(4),parameter::cdi_key_stdname=944_4
integer(4),parameter::cdi_key_units=945_4
integer(4),parameter::cdi_key_datatype=946_4
integer(4),parameter::cdi_key_referenceuri=947_4
integer(4),parameter::cdi_key_chunks=948_4
integer(4),parameter::cdi_key_numberofgridused=961_4
integer(4),parameter::cdi_key_numberofgridinreference=962_4
integer(4),parameter::cdi_key_numberofvgridused=963_4
integer(4),parameter::cdi_key_nlev=964_4
integer(4),parameter::cdi_key_chunktype=965_4
integer(4),parameter::cdi_key_chunksize=966_4
integer(4),parameter::cdi_key_missval=701_4
integer(4),parameter::cdi_key_addoffset=702_4
integer(4),parameter::cdi_key_scalefactor=703_4
integer(4),parameter::cdi_key_uuid=960_4
integer(4),parameter::cdi_key_dimname=941_4
integer(4),parameter::cdi_key_psname=950_4
integer(4),parameter::cdi_key_p0name=951_4
integer(4),parameter::cdi_key_p0value=952_4
integer(4),parameter::cdi_key_tablesversion=801_4
integer(4),parameter::cdi_key_localtablesversion=802_4
integer(4),parameter::cdi_key_typeofgeneratingprocess=803_4
integer(4),parameter::cdi_key_productdefinitiontemplate=804_4
integer(4),parameter::cdi_key_typeofprocesseddata=805_4
integer(4),parameter::cdi_key_shapeoftheearth=806_4
integer(4),parameter::cdi_key_backgroundprocess=807_4
integer(4),parameter::cdi_key_typeofensembleforecast=808_4
integer(4),parameter::cdi_key_numberofforecastsinensemble=809_4
integer(4),parameter::cdi_key_perturbationnumber=810_4
integer(4),parameter::cdi_key_centre=811_4
integer(4),parameter::cdi_key_subcentre=812_4
integer(4),parameter::cdi_key_mpimtype=813_4
integer(4),parameter::cdi_key_mpimclass=814_4
integer(4),parameter::cdi_key_mpimuser=815_4
integer(4),parameter::cdi_key_revstatus=816_4
integer(4),parameter::cdi_key_revnumber=817_4
integer(4),parameter::cdi_key_grib2localsectionnumber=818_4
integer(4),parameter::cdi_key_section2paddinglength=819_4
integer(4),parameter::cdi_key_section2padding=820_4
integer(4),parameter::cdi_key_constituenttype=821_4
integer(4),parameter::cdi_key_typeoftimeincrement=822_4
integer(4),parameter::cdi_key_typeoffirstfixedsurface=823_4
integer(4),parameter::cdi_key_typeofsecondfixedsurface=824_4
integer(4),parameter::cdi_key_uvrelativetogrid=825_4
integer(4),parameter::cdi_key_scanningmode=826_4
integer(4),parameter::cdi_key_vdimname=920_4
integer(4),parameter::cdi_key_gridmap_vartype=921_4
integer(4),parameter::cdi_key_gridmap_varname=922_4
integer(4),parameter::cdi_key_gridmap_name=923_4
integer(4),parameter::have_cdi_proj_funcs=1_4
interface
subroutine cdireset() bind(c,name="cdiReset")
end
end interface
interface
subroutine cdidebug(debug_dummy) bind(c,name="cdiDebug")
integer(4),value::debug_dummy
end
end interface
interface
subroutine cdiprintversion() bind(c,name="cdiPrintVersion")
end
end interface
interface
function cdihavefiletype(filetype_dummy) bind(c,name="cdiHaveFiletype") result(f_result)
integer(4),value::filetype_dummy
integer(4)::f_result
end
end interface
interface
subroutine cdidefmissval(missval_dummy) bind(c,name="cdiDefMissval")
real(8),value::missval_dummy
end
end interface
interface
function cdiinqmissval() bind(c,name="cdiInqMissval") result(f_result)
real(8)::f_result
end
end interface
interface
function namespacenew() bind(c,name="namespaceNew") result(f_result)
integer(4)::f_result
end
end interface
interface
subroutine namespacesetactive(namespaceid_dummy) bind(c,name="namespaceSetActive")
integer(4),value::namespaceid_dummy
end
end interface
interface
function namespacegetactive() bind(c,name="namespaceGetActive") result(f_result)
integer(4)::f_result
end
end interface
interface
subroutine namespacedelete(namespaceid_dummy) bind(c,name="namespaceDelete")
integer(4),value::namespaceid_dummy
end
end interface
interface
subroutine cdidecodeparam(param_dummy,pnum_dummy,pcat_dummy,pdis_dummy) bind(c,name="cdiDecodeParam")
integer(4),value::param_dummy
integer(4),intent(inout)::pnum_dummy
integer(4),intent(inout)::pcat_dummy
integer(4),intent(inout)::pdis_dummy
end
end interface
interface
function cdiencodeparam(pnum_dummy,pcat_dummy,pdis_dummy) bind(c,name="cdiEncodeParam") result(f_result)
integer(4),value::pnum_dummy
integer(4),value::pcat_dummy
integer(4),value::pdis_dummy
integer(4)::f_result
end
end interface
interface
subroutine cdidecodedate(date_dummy,year_dummy,month_dummy,day_dummy) bind(c,name="cdiDecodeDate")
integer(4),value::date_dummy
integer(4),intent(inout)::year_dummy
integer(4),intent(inout)::month_dummy
integer(4),intent(inout)::day_dummy
end
end interface
interface
function cdiencodedate(year_dummy,month_dummy,day_dummy) bind(c,name="cdiEncodeDate") result(f_result)
integer(4),value::year_dummy
integer(4),value::month_dummy
integer(4),value::day_dummy
integer(4)::f_result
end
end interface
interface
subroutine cdidecodetime(time_dummy,hour_dummy,minute_dummy,second_dummy) bind(c,name="cdiDecodeTime")
integer(4),value::time_dummy
integer(4),intent(inout)::hour_dummy
integer(4),intent(inout)::minute_dummy
integer(4),intent(inout)::second_dummy
end
end interface
interface
function cdiencodetime(hour_dummy,minute_dummy,second_dummy) bind(c,name="cdiEncodeTime") result(f_result)
integer(4),value::hour_dummy
integer(4),value::minute_dummy
integer(4),value::second_dummy
integer(4)::f_result
end
end interface
interface
subroutine streamclose(streamid_dummy) bind(c,name="streamClose")
integer(4),value::streamid_dummy
end
end interface
interface
subroutine streamsync(streamid_dummy) bind(c,name="streamSync")
integer(4),value::streamid_dummy
end
end interface
interface
subroutine streamdefmaxsteps(streamid_dummy,maxsteps_dummy) bind(c,name="streamDefMaxSteps")
integer(4),value::streamid_dummy
integer(4),value::maxsteps_dummy
end
end interface
interface
subroutine streamdefnumworker(streamid_dummy,numworker_dummy) bind(c,name="streamDefNumWorker")
integer(4),value::streamid_dummy
integer(4),value::numworker_dummy
end
end interface
interface
function streaminqnumsteps(streamid_dummy) bind(c,name="streamInqNumSteps") result(f_result)
integer(4),value::streamid_dummy
integer(4)::f_result
end
end interface
interface
subroutine streamdefvlist(streamid_dummy,vlistid_dummy) bind(c,name="streamDefVlist")
integer(4),value::streamid_dummy
integer(4),value::vlistid_dummy
end
end interface
interface
function streaminqvlist(streamid_dummy) bind(c,name="streamInqVlist") result(f_result)
integer(4),value::streamid_dummy
integer(4)::f_result
end
end interface
interface
function streaminqfiletype(streamid_dummy) bind(c,name="streamInqFiletype") result(f_result)
integer(4),value::streamid_dummy
integer(4)::f_result
end
end interface
interface
subroutine streamdefbyteorder(streamid_dummy,byteorder_dummy) bind(c,name="streamDefByteorder")
integer(4),value::streamid_dummy
integer(4),value::byteorder_dummy
end
end interface
interface
function streaminqbyteorder(streamid_dummy) bind(c,name="streamInqByteorder") result(f_result)
integer(4),value::streamid_dummy
integer(4)::f_result
end
end interface
interface
subroutine streamdefshuffle(streamid_dummy,shuffle_dummy) bind(c,name="streamDefShuffle")
integer(4),value::streamid_dummy
integer(4),value::shuffle_dummy
end
end interface
interface
subroutine streamdefcomptype(streamid_dummy,comptype_dummy) bind(c,name="streamDefCompType")
integer(4),value::streamid_dummy
integer(4),value::comptype_dummy
end
end interface
interface
function streaminqcomptype(streamid_dummy) bind(c,name="streamInqCompType") result(f_result)
integer(4),value::streamid_dummy
integer(4)::f_result
end
end interface
interface
subroutine streamdefcomplevel(streamid_dummy,complevel_dummy) bind(c,name="streamDefCompLevel")
integer(4),value::streamid_dummy
integer(4),value::complevel_dummy
end
end interface
interface
function streaminqcomplevel(streamid_dummy) bind(c,name="streamInqCompLevel") result(f_result)
integer(4),value::streamid_dummy
integer(4)::f_result
end
end interface
interface
function streamdeftimestep(streamid_dummy,tsid_dummy) bind(c,name="streamDefTimestep") result(f_result)
integer(4),value::streamid_dummy
integer(4),value::tsid_dummy
integer(4)::f_result
end
end interface
interface
function streaminqtimestep(streamid_dummy,tsid_dummy) bind(c,name="streamInqTimestep") result(f_result)
integer(4),value::streamid_dummy
integer(4),value::tsid_dummy
integer(4)::f_result
end
end interface
interface
function streaminqcurtimestepid(streamid_dummy) bind(c,name="streamInqCurTimestepID") result(f_result)
integer(4),value::streamid_dummy
integer(4)::f_result
end
end interface
interface
function streamnvals(streamid_dummy) bind(c,name="streamNvals") result(f_result)
integer(4),value::streamid_dummy
integer(4)::f_result
end
end interface
interface
function streaminqnvars(streamid_dummy) bind(c,name="streamInqNvars") result(f_result)
integer(4),value::streamid_dummy
integer(4)::f_result
end
end interface
interface
subroutine streamwritevar(streamid_dummy,varid_dummy,data_dummy,nmiss_dummy) bind(c,name="streamWriteVar")
integer(4),value::streamid_dummy
integer(4),value::varid_dummy
real(8),intent(in)::data_dummy(1_8:*)
integer(4),value::nmiss_dummy
end
end interface
interface
subroutine streamwritevarf(streamid_dummy,varid_dummy,data_dummy,nmiss_dummy) bind(c,name="streamWriteVarF")
integer(4),value::streamid_dummy
integer(4),value::varid_dummy
real(4),intent(in)::data_dummy(1_8:*)
integer(4),value::nmiss_dummy
end
end interface
interface
subroutine streamreadvar(streamid_dummy,varid_dummy,data_dummy,nmiss_dummy) bind(c,name="streamReadVar")
integer(4),value::streamid_dummy
integer(4),value::varid_dummy
real(8),intent(inout)::data_dummy(1_8:*)
integer(4),intent(inout)::nmiss_dummy
end
end interface
interface
subroutine streamreadvarf(streamid_dummy,varid_dummy,data_dummy,nmiss_dummy) bind(c,name="streamReadVarF")
integer(4),value::streamid_dummy
integer(4),value::varid_dummy
real(4),intent(inout)::data_dummy(1_8:*)
integer(4),intent(inout)::nmiss_dummy
end
end interface
interface
subroutine streamwritevarslice(streamid_dummy,varid_dummy,levelid_dummy,data_dummy,nmiss_dummy) bind(c,name="streamWriteVarSlice")
integer(4),value::streamid_dummy
integer(4),value::varid_dummy
integer(4),value::levelid_dummy
real(8),intent(in)::data_dummy(1_8:*)
integer(4),value::nmiss_dummy
end
end interface
interface
subroutine streamwritevarslicef(streamid_dummy,varid_dummy,levelid_dummy,data_dummy,nmiss_dummy) bind(c,name="streamWriteVarSliceF")
integer(4),value::streamid_dummy
integer(4),value::varid_dummy
integer(4),value::levelid_dummy
real(4),intent(in)::data_dummy(1_8:*)
integer(4),value::nmiss_dummy
end
end interface
interface
subroutine streamreadvarslice(streamid_dummy,varid_dummy,levelid_dummy,data_dummy,nmiss_dummy) bind(c,name="streamReadVarSlice")
integer(4),value::streamid_dummy
integer(4),value::varid_dummy
integer(4),value::levelid_dummy
real(8),intent(inout)::data_dummy(1_8:*)
integer(4),intent(inout)::nmiss_dummy
end
end interface
interface
subroutine streamreadvarslicef(streamid_dummy,varid_dummy,levelid_dummy,data_dummy,nmiss_dummy) bind(c,name="streamReadVarSliceF")
integer(4),value::streamid_dummy
integer(4),value::varid_dummy
integer(4),value::levelid_dummy
real(4),intent(inout)::data_dummy(1_8:*)
integer(4),intent(inout)::nmiss_dummy
end
end interface
interface
subroutine streamwritevarchunk(streamid_dummy,varid_dummy,rect_dummy,data_dummy,nmiss_dummy) bind(c,name="streamWriteVarChunk")
integer(4),value::streamid_dummy
integer(4),value::varid_dummy
integer(4),intent(in)::rect_dummy(1_8:2_8,1_8:*)
real(8),intent(in)::data_dummy(1_8:*)
integer(4),value::nmiss_dummy
end
end interface
interface
subroutine streamwritevarchunkf(streamid_dummy,varid_dummy,rect_dummy,data_dummy,nmiss_dummy) bind(c,name="streamWriteVarChunkF")
integer(4),value::streamid_dummy
integer(4),value::varid_dummy
integer(4),intent(in)::rect_dummy(1_8:2_8,1_8:*)
real(4),intent(in)::data_dummy(1_8:*)
integer(4),value::nmiss_dummy
end
end interface
interface
subroutine streamdefrecord(streamid_dummy,varid_dummy,levelid_dummy) bind(c,name="streamDefRecord")
integer(4),value::streamid_dummy
integer(4),value::varid_dummy
integer(4),value::levelid_dummy
end
end interface
interface
subroutine streaminqrecord(streamid_dummy,varid_dummy,levelid_dummy) bind(c,name="streamInqRecord")
integer(4),value::streamid_dummy
integer(4),intent(inout)::varid_dummy
integer(4),intent(inout)::levelid_dummy
end
end interface
interface
subroutine streamwriterecord(streamid_dummy,data_dummy,nmiss_dummy) bind(c,name="streamWriteRecord")
integer(4),value::streamid_dummy
real(8),intent(in)::data_dummy(1_8:*)
integer(4),value::nmiss_dummy
end
end interface
interface
subroutine streamwriterecordf(streamid_dummy,data_dummy,nmiss_dummy) bind(c,name="streamWriteRecordF")
integer(4),value::streamid_dummy
real(4),intent(in)::data_dummy(1_8:*)
integer(4),value::nmiss_dummy
end
end interface
interface
subroutine streamreadrecord(streamid_dummy,data_dummy,nmiss_dummy) bind(c,name="streamReadRecord")
integer(4),value::streamid_dummy
real(8),intent(inout)::data_dummy(1_8:*)
integer(4),intent(inout)::nmiss_dummy
end
end interface
interface
subroutine streamreadrecordf(streamid_dummy,data_dummy,nmiss_dummy) bind(c,name="streamReadRecordF")
integer(4),value::streamid_dummy
real(4),intent(inout)::data_dummy(1_8:*)
integer(4),intent(inout)::nmiss_dummy
end
end interface
interface
subroutine streamcopyrecord(streamiddest_dummy,streamidsrc_dummy) bind(c,name="streamCopyRecord")
integer(4),value::streamiddest_dummy
integer(4),value::streamidsrc_dummy
end
end interface
interface
function vlistcreate() bind(c,name="vlistCreate") result(f_result)
integer(4)::f_result
end
end interface
interface
subroutine vlistdestroy(vlistid_dummy) bind(c,name="vlistDestroy")
integer(4),value::vlistid_dummy
end
end interface
interface
function vlistduplicate(vlistid_dummy) bind(c,name="vlistDuplicate") result(f_result)
integer(4),value::vlistid_dummy
integer(4)::f_result
end
end interface
interface
subroutine vlistcopy(vlistid2_dummy,vlistid1_dummy) bind(c,name="vlistCopy")
integer(4),value::vlistid2_dummy
integer(4),value::vlistid1_dummy
end
end interface
interface
subroutine vlistcopyflag(vlistid2_dummy,vlistid1_dummy) bind(c,name="vlistCopyFlag")
integer(4),value::vlistid2_dummy
integer(4),value::vlistid1_dummy
end
end interface
interface
subroutine vlistclearflag(vlistid_dummy) bind(c,name="vlistClearFlag")
integer(4),value::vlistid_dummy
end
end interface
interface
subroutine vlistcat(vlistid2_dummy,vlistid1_dummy) bind(c,name="vlistCat")
integer(4),value::vlistid2_dummy
integer(4),value::vlistid1_dummy
end
end interface
interface
subroutine vlistmerge(vlistid2_dummy,vlistid1_dummy) bind(c,name="vlistMerge")
integer(4),value::vlistid2_dummy
integer(4),value::vlistid1_dummy
end
end interface
interface
subroutine vlistprint(vlistid_dummy) bind(c,name="vlistPrint")
integer(4),value::vlistid_dummy
end
end interface
interface
function vlistnumber(vlistid_dummy) bind(c,name="vlistNumber") result(f_result)
integer(4),value::vlistid_dummy
integer(4)::f_result
end
end interface
interface
function vlistnvars(vlistid_dummy) bind(c,name="vlistNvars") result(f_result)
integer(4),value::vlistid_dummy
integer(4)::f_result
end
end interface
interface
function vlistngrids(vlistid_dummy) bind(c,name="vlistNgrids") result(f_result)
integer(4),value::vlistid_dummy
integer(4)::f_result
end
end interface
interface
function vlistnzaxis(vlistid_dummy) bind(c,name="vlistNzaxis") result(f_result)
integer(4),value::vlistid_dummy
integer(4)::f_result
end
end interface
interface
function vlistnsubtypes(vlistid_dummy) bind(c,name="vlistNsubtypes") result(f_result)
integer(4),value::vlistid_dummy
integer(4)::f_result
end
end interface
interface
subroutine vlistdefntsteps(vlistid_dummy,nts_dummy) bind(c,name="vlistDefNtsteps")
integer(4),value::vlistid_dummy
integer(4),value::nts_dummy
end
end interface
interface
function vlistntsteps(vlistid_dummy) bind(c,name="vlistNtsteps") result(f_result)
integer(4),value::vlistid_dummy
integer(4)::f_result
end
end interface
interface
function vlistgridsizemax(vlistid_dummy) bind(c,name="vlistGridsizeMax") result(f_result)
integer(4),value::vlistid_dummy
integer(4)::f_result
end
end interface
interface
function vlistgrid(vlistid_dummy,index_dummy) bind(c,name="vlistGrid") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::index_dummy
integer(4)::f_result
end
end interface
interface
function vlistgridindex(vlistid_dummy,gridid_dummy) bind(c,name="vlistGridIndex") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::gridid_dummy
integer(4)::f_result
end
end interface
interface
subroutine vlistchangegridindex(vlistid_dummy,index_dummy,gridid_dummy) bind(c,name="vlistChangeGridIndex")
integer(4),value::vlistid_dummy
integer(4),value::index_dummy
integer(4),value::gridid_dummy
end
end interface
interface
subroutine vlistchangegrid(vlistid_dummy,gridid1_dummy,gridid2_dummy) bind(c,name="vlistChangeGrid")
integer(4),value::vlistid_dummy
integer(4),value::gridid1_dummy
integer(4),value::gridid2_dummy
end
end interface
interface
function vlistzaxis(vlistid_dummy,index_dummy) bind(c,name="vlistZaxis") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::index_dummy
integer(4)::f_result
end
end interface
interface
function vlistzaxisindex(vlistid_dummy,zaxisid_dummy) bind(c,name="vlistZaxisIndex") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::zaxisid_dummy
integer(4)::f_result
end
end interface
interface
subroutine vlistchangezaxisindex(vlistid_dummy,index_dummy,zaxisid_dummy) bind(c,name="vlistChangeZaxisIndex")
integer(4),value::vlistid_dummy
integer(4),value::index_dummy
integer(4),value::zaxisid_dummy
end
end interface
interface
subroutine vlistchangezaxis(vlistid_dummy,zaxisid1_dummy,zaxisid2_dummy) bind(c,name="vlistChangeZaxis")
integer(4),value::vlistid_dummy
integer(4),value::zaxisid1_dummy
integer(4),value::zaxisid2_dummy
end
end interface
interface
function vlistnrecs(vlistid_dummy) bind(c,name="vlistNrecs") result(f_result)
integer(4),value::vlistid_dummy
integer(4)::f_result
end
end interface
interface
function vlistsubtype(vlistid_dummy,index_dummy) bind(c,name="vlistSubtype") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::index_dummy
integer(4)::f_result
end
end interface
interface
function vlistsubtypeindex(vlistid_dummy,subtypeid_dummy) bind(c,name="vlistSubtypeIndex") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::subtypeid_dummy
integer(4)::f_result
end
end interface
interface
subroutine vlistdeftaxis(vlistid_dummy,taxisid_dummy) bind(c,name="vlistDefTaxis")
integer(4),value::vlistid_dummy
integer(4),value::taxisid_dummy
end
end interface
interface
function vlistinqtaxis(vlistid_dummy) bind(c,name="vlistInqTaxis") result(f_result)
integer(4),value::vlistid_dummy
integer(4)::f_result
end
end interface
interface
subroutine vlistdeftable(vlistid_dummy,tableid_dummy) bind(c,name="vlistDefTable")
integer(4),value::vlistid_dummy
integer(4),value::tableid_dummy
end
end interface
interface
function vlistinqtable(vlistid_dummy) bind(c,name="vlistInqTable") result(f_result)
integer(4),value::vlistid_dummy
integer(4)::f_result
end
end interface
interface
subroutine vlistdefinstitut(vlistid_dummy,instid_dummy) bind(c,name="vlistDefInstitut")
integer(4),value::vlistid_dummy
integer(4),value::instid_dummy
end
end interface
interface
function vlistinqinstitut(vlistid_dummy) bind(c,name="vlistInqInstitut") result(f_result)
integer(4),value::vlistid_dummy
integer(4)::f_result
end
end interface
interface
subroutine vlistdefmodel(vlistid_dummy,modelid_dummy) bind(c,name="vlistDefModel")
integer(4),value::vlistid_dummy
integer(4),value::modelid_dummy
end
end interface
interface
function vlistinqmodel(vlistid_dummy) bind(c,name="vlistInqModel") result(f_result)
integer(4),value::vlistid_dummy
integer(4)::f_result
end
end interface
interface
function vlistdefvartiles(vlistid_dummy,gridid_dummy,zaxisid_dummy,timetype_dummy,tilesetid_dummy) bind(c,name="vlistDefVarTiles") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::gridid_dummy
integer(4),value::zaxisid_dummy
integer(4),value::timetype_dummy
integer(4),value::tilesetid_dummy
integer(4)::f_result
end
end interface
interface
function vlistdefvar(vlistid_dummy,gridid_dummy,zaxisid_dummy,timetype_dummy) bind(c,name="vlistDefVar") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::gridid_dummy
integer(4),value::zaxisid_dummy
integer(4),value::timetype_dummy
integer(4)::f_result
end
end interface
interface
subroutine vlistchangevargrid(vlistid_dummy,varid_dummy,gridid_dummy) bind(c,name="vlistChangeVarGrid")
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4),value::gridid_dummy
end
end interface
interface
subroutine vlistchangevarzaxis(vlistid_dummy,varid_dummy,zaxisid_dummy) bind(c,name="vlistChangeVarZaxis")
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4),value::zaxisid_dummy
end
end interface
interface
subroutine vlistinqvar(vlistid_dummy,varid_dummy,gridid_dummy,zaxisid_dummy,timetype_dummy) bind(c,name="vlistInqVar")
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4),intent(inout)::gridid_dummy
integer(4),intent(inout)::zaxisid_dummy
integer(4),intent(inout)::timetype_dummy
end
end interface
interface
function vlistinqvargrid(vlistid_dummy,varid_dummy) bind(c,name="vlistInqVarGrid") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4)::f_result
end
end interface
interface
function vlistinqvarzaxis(vlistid_dummy,varid_dummy) bind(c,name="vlistInqVarZaxis") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4)::f_result
end
end interface
interface
function vlistinqvarid(vlistid_dummy,code_dummy) bind(c,name="vlistInqVarID") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::code_dummy
integer(4)::f_result
end
end interface
interface
subroutine vlistdefvartimetype(vlistid_dummy,varid_dummy,timetype_dummy) bind(c,name="vlistDefVarTimetype")
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4),value::timetype_dummy
end
end interface
interface
function vlistinqvartimetype(vlistid_dummy,varid_dummy) bind(c,name="vlistInqVarTimetype") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4)::f_result
end
end interface
interface
subroutine vlistdefvartsteptype(vlistid_dummy,varid_dummy,tsteptype_dummy) bind(c,name="vlistDefVarTsteptype")
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4),value::tsteptype_dummy
end
end interface
interface
function vlistinqvartsteptype(vlistid_dummy,varid_dummy) bind(c,name="vlistInqVarTsteptype") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4)::f_result
end
end interface
interface
subroutine vlistdefvarcomptype(vlistid_dummy,varid_dummy,comptype_dummy) bind(c,name="vlistDefVarCompType")
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4),value::comptype_dummy
end
end interface
interface
function vlistinqvarcomptype(vlistid_dummy,varid_dummy) bind(c,name="vlistInqVarCompType") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4)::f_result
end
end interface
interface
subroutine vlistdefvarcomplevel(vlistid_dummy,varid_dummy,complevel_dummy) bind(c,name="vlistDefVarCompLevel")
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4),value::complevel_dummy
end
end interface
interface
function vlistinqvarcomplevel(vlistid_dummy,varid_dummy) bind(c,name="vlistInqVarCompLevel") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4)::f_result
end
end interface
interface
subroutine vlistdefvarparam(vlistid_dummy,varid_dummy,param_dummy) bind(c,name="vlistDefVarParam")
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4),value::param_dummy
end
end interface
interface
function vlistinqvarparam(vlistid_dummy,varid_dummy) bind(c,name="vlistInqVarParam") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4)::f_result
end
end interface
interface
subroutine vlistdefvarcode(vlistid_dummy,varid_dummy,code_dummy) bind(c,name="vlistDefVarCode")
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4),value::code_dummy
end
end interface
interface
function vlistinqvarcode(vlistid_dummy,varid_dummy) bind(c,name="vlistInqVarCode") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4)::f_result
end
end interface
interface
subroutine vlistdefvardatatype(vlistid_dummy,varid_dummy,datatype_dummy) bind(c,name="vlistDefVarDatatype")
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4),value::datatype_dummy
end
end interface
interface
function vlistinqvardatatype(vlistid_dummy,varid_dummy) bind(c,name="vlistInqVarDatatype") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4)::f_result
end
end interface
interface
subroutine vlistdefvarxyz(vlistid_dummy,varid_dummy,xyz_dummy) bind(c,name="vlistDefVarXYZ")
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4),value::xyz_dummy
end
end interface
interface
function vlistinqvarxyz(vlistid_dummy,varid_dummy) bind(c,name="vlistInqVarXYZ") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4)::f_result
end
end interface
interface
subroutine vlistdefvarnsb(vlistid_dummy,varid_dummy,nsb_dummy) bind(c,name="vlistDefVarNSB")
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4),value::nsb_dummy
end
end interface
interface
function vlistinqvarnsb(vlistid_dummy,varid_dummy) bind(c,name="vlistInqVarNSB") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4)::f_result
end
end interface
interface
function vlistinqvarnumber(vlistid_dummy,varid_dummy) bind(c,name="vlistInqVarNumber") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4)::f_result
end
end interface
interface
subroutine vlistdefvarinstitut(vlistid_dummy,varid_dummy,instid_dummy) bind(c,name="vlistDefVarInstitut")
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4),value::instid_dummy
end
end interface
interface
function vlistinqvarinstitut(vlistid_dummy,varid_dummy) bind(c,name="vlistInqVarInstitut") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4)::f_result
end
end interface
interface
subroutine vlistdefvarmodel(vlistid_dummy,varid_dummy,modelid_dummy) bind(c,name="vlistDefVarModel")
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4),value::modelid_dummy
end
end interface
interface
function vlistinqvarmodel(vlistid_dummy,varid_dummy) bind(c,name="vlistInqVarModel") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4)::f_result
end
end interface
interface
subroutine vlistdefvartable(vlistid_dummy,varid_dummy,tableid_dummy) bind(c,name="vlistDefVarTable")
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4),value::tableid_dummy
end
end interface
interface
function vlistinqvartable(vlistid_dummy,varid_dummy) bind(c,name="vlistInqVarTable") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4)::f_result
end
end interface
interface
subroutine vlistdefvarmissval(vlistid_dummy,varid_dummy,missval_dummy) bind(c,name="vlistDefVarMissval")
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
real(8),value::missval_dummy
end
end interface
interface
function vlistinqvarmissval(vlistid_dummy,varid_dummy) bind(c,name="vlistInqVarMissval") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
real(8)::f_result
end
end interface
interface
function vlistinqvarsize(vlistid_dummy,varid_dummy) bind(c,name="vlistInqVarSize") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4)::f_result
end
end interface
interface
subroutine vlistdefindex(vlistid_dummy,varid_dummy,levid_dummy,index_dummy) bind(c,name="vlistDefIndex")
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4),value::levid_dummy
integer(4),value::index_dummy
end
end interface
interface
function vlistinqindex(vlistid_dummy,varid_dummy,levid_dummy) bind(c,name="vlistInqIndex") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4),value::levid_dummy
integer(4)::f_result
end
end interface
interface
subroutine vlistdefflag(vlistid_dummy,varid_dummy,levid_dummy,flag_dummy) bind(c,name="vlistDefFlag")
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4),value::levid_dummy
integer(4),value::flag_dummy
end
end interface
interface
function vlistinqflag(vlistid_dummy,varid_dummy,levid_dummy) bind(c,name="vlistInqFlag") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4),value::levid_dummy
integer(4)::f_result
end
end interface
interface
function vlistfindvar(vlistid_dummy,fvarid_dummy) bind(c,name="vlistFindVar") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::fvarid_dummy
integer(4)::f_result
end
end interface
interface
function vlistfindlevel(vlistid_dummy,fvarid_dummy,flevelid_dummy) bind(c,name="vlistFindLevel") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::fvarid_dummy
integer(4),value::flevelid_dummy
integer(4)::f_result
end
end interface
interface
function vlistmergedvar(vlistid_dummy,varid_dummy) bind(c,name="vlistMergedVar") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4)::f_result
end
end interface
interface
function vlistmergedlevel(vlistid_dummy,varid_dummy,levelid_dummy) bind(c,name="vlistMergedLevel") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4),value::levelid_dummy
integer(4)::f_result
end
end interface
interface
subroutine cdiclearadditionalkeys() bind(c,name="cdiClearAdditionalKeys")
end
end interface
interface
function cdiinqnatts(cdiid_dummy,varid_dummy,nattsp_dummy) bind(c,name="cdiInqNatts") result(f_result)
integer(4),value::cdiid_dummy
integer(4),value::varid_dummy
integer(4),intent(inout)::nattsp_dummy
integer(4)::f_result
end
end interface
interface
function cdicopyatts(cdiid1_dummy,varid1_dummy,cdiid2_dummy,varid2_dummy) bind(c,name="cdiCopyAtts") result(f_result)
integer(4),value::cdiid1_dummy
integer(4),value::varid1_dummy
integer(4),value::cdiid2_dummy
integer(4),value::varid2_dummy
integer(4)::f_result
end
end interface
interface
subroutine gridcompress(gridid_dummy) bind(c,name="gridCompress")
integer(4),value::gridid_dummy
end
end interface
interface
subroutine griddefmaskgme(gridid_dummy,mask_dummy) bind(c,name="gridDefMaskGME")
integer(4),value::gridid_dummy
integer(4),intent(in)::mask_dummy(1_8:*)
end
end interface
interface
function gridinqmaskgme(gridid_dummy,mask_dummy) bind(c,name="gridInqMaskGME") result(f_result)
integer(4),value::gridid_dummy
integer(4),intent(inout)::mask_dummy(1_8:*)
integer(4)::f_result
end
end interface
interface
subroutine griddefmask(gridid_dummy,mask_dummy) bind(c,name="gridDefMask")
integer(4),value::gridid_dummy
integer(4),intent(in)::mask_dummy(1_8:*)
end
end interface
interface
function gridinqmask(gridid_dummy,mask_dummy) bind(c,name="gridInqMask") result(f_result)
integer(4),value::gridid_dummy
integer(4),intent(inout)::mask_dummy(1_8:*)
integer(4)::f_result
end
end interface
interface
function gridcreate(gridtype_dummy,size_dummy) bind(c,name="gridCreate") result(f_result)
integer(4),value::gridtype_dummy
integer(4),value::size_dummy
integer(4)::f_result
end
end interface
interface
subroutine griddestroy(gridid_dummy) bind(c,name="gridDestroy")
integer(4),value::gridid_dummy
end
end interface
interface
function gridduplicate(gridid_dummy) bind(c,name="gridDuplicate") result(f_result)
integer(4),value::gridid_dummy
integer(4)::f_result
end
end interface
interface
subroutine griddefproj(gridid_dummy,projid_dummy) bind(c,name="gridDefProj")
integer(4),value::gridid_dummy
integer(4),value::projid_dummy
end
end interface
interface
function gridinqproj(gridid_dummy) bind(c,name="gridInqProj") result(f_result)
integer(4),value::gridid_dummy
integer(4)::f_result
end
end interface
interface
function gridinqprojtype(gridid_dummy) bind(c,name="gridInqProjType") result(f_result)
integer(4),value::gridid_dummy
integer(4)::f_result
end
end interface
interface
function gridinqtype(gridid_dummy) bind(c,name="gridInqType") result(f_result)
integer(4),value::gridid_dummy
integer(4)::f_result
end
end interface
interface
function gridinqsize(gridid_dummy) bind(c,name="gridInqSize") result(f_result)
integer(4),value::gridid_dummy
integer(4)::f_result
end
end interface
interface
subroutine griddefxsize(gridid_dummy,xsize_dummy) bind(c,name="gridDefXsize")
integer(4),value::gridid_dummy
integer(4),value::xsize_dummy
end
end interface
interface
function gridinqxsize(gridid_dummy) bind(c,name="gridInqXsize") result(f_result)
integer(4),value::gridid_dummy
integer(4)::f_result
end
end interface
interface
subroutine griddefysize(gridid_dummy,ysize_dummy) bind(c,name="gridDefYsize")
integer(4),value::gridid_dummy
integer(4),value::ysize_dummy
end
end interface
interface
function gridinqysize(gridid_dummy) bind(c,name="gridInqYsize") result(f_result)
integer(4),value::gridid_dummy
integer(4)::f_result
end
end interface
interface
subroutine griddefnp(gridid_dummy,np_dummy) bind(c,name="gridDefNP")
integer(4),value::gridid_dummy
integer(4),value::np_dummy
end
end interface
interface
function gridinqnp(gridid_dummy) bind(c,name="gridInqNP") result(f_result)
integer(4),value::gridid_dummy
integer(4)::f_result
end
end interface
interface
subroutine griddefxvals(gridid_dummy,xvals_dummy) bind(c,name="gridDefXvals")
integer(4),value::gridid_dummy
real(8),intent(in)::xvals_dummy(1_8:*)
end
end interface
interface
function gridinqxvals(gridid_dummy,xvals_dummy) bind(c,name="gridInqXvals") result(f_result)
integer(4),value::gridid_dummy
real(8),intent(inout)::xvals_dummy(1_8:*)
integer(4)::f_result
end
end interface
interface
function gridinqxvalspart(gridid_dummy,start_dummy,size_dummy,xvals_dummy) bind(c,name="gridInqXvalsPart") result(f_result)
integer(4),value::gridid_dummy
integer(4),value::start_dummy
integer(4),value::size_dummy
real(8),intent(inout)::xvals_dummy(1_8:*)
integer(4)::f_result
end
end interface
interface
function gridinqxvalsptr(gridid_dummy) bind(c,name="gridInqXvalsPtr") result(f_result)
import::c_ptr
integer(4),value::gridid_dummy
type(c_ptr)::f_result
end
end interface
interface
function gridinqxisc(gridid_dummy) bind(c,name="gridInqXIsc") result(f_result)
integer(4),value::gridid_dummy
integer(4)::f_result
end
end interface
interface
subroutine griddefyvals(gridid_dummy,yvals_dummy) bind(c,name="gridDefYvals")
integer(4),value::gridid_dummy
real(8),intent(in)::yvals_dummy(1_8:*)
end
end interface
interface
function gridinqyvals(gridid_dummy,yvals_dummy) bind(c,name="gridInqYvals") result(f_result)
integer(4),value::gridid_dummy
real(8),intent(inout)::yvals_dummy(1_8:*)
integer(4)::f_result
end
end interface
interface
function gridinqyvalspart(gridid_dummy,start_dummy,size_dummy,yvals_dummy) bind(c,name="gridInqYvalsPart") result(f_result)
integer(4),value::gridid_dummy
integer(4),value::start_dummy
integer(4),value::size_dummy
real(8),intent(inout)::yvals_dummy(1_8:*)
integer(4)::f_result
end
end interface
interface
function gridinqyvalsptr(gridid_dummy) bind(c,name="gridInqYvalsPtr") result(f_result)
import::c_ptr
integer(4),value::gridid_dummy
type(c_ptr)::f_result
end
end interface
interface
function gridinqyisc(gridid_dummy) bind(c,name="gridInqYIsc") result(f_result)
integer(4),value::gridid_dummy
integer(4)::f_result
end
end interface
interface
function cdidefkeyint(cdiid_dummy,varid_dummy,key_dummy,value_dummy) bind(c,name="cdiDefKeyInt") result(f_result)
integer(4),value::cdiid_dummy
integer(4),value::varid_dummy
integer(4),value::key_dummy
integer(4),value::value_dummy
integer(4)::f_result
end
end interface
interface
function cdiinqkeyint(cdiid_dummy,varid_dummy,key_dummy,value_dummy) bind(c,name="cdiInqKeyInt") result(f_result)
integer(4),value::cdiid_dummy
integer(4),value::varid_dummy
integer(4),value::key_dummy
integer(4),intent(inout)::value_dummy
integer(4)::f_result
end
end interface
interface
function cdidefkeyfloat(cdiid_dummy,varid_dummy,key_dummy,value_dummy) bind(c,name="cdiDefKeyFloat") result(f_result)
integer(4),value::cdiid_dummy
integer(4),value::varid_dummy
integer(4),value::key_dummy
real(8),value::value_dummy
integer(4)::f_result
end
end interface
interface
function cdiinqkeyfloat(cdiid_dummy,varid_dummy,key_dummy,value_dummy) bind(c,name="cdiInqKeyFloat") result(f_result)
integer(4),value::cdiid_dummy
integer(4),value::varid_dummy
integer(4),value::key_dummy
real(8),intent(inout)::value_dummy
integer(4)::f_result
end
end interface
interface
function cdidefkeybytes(cdiid_dummy,varid_dummy,key_dummy,bytes_dummy,length_dummy) bind(c,name="cdiDefKeyBytes") result(f_result)
integer(4),value::cdiid_dummy
integer(4),value::varid_dummy
integer(4),value::key_dummy
integer(1),intent(in)::bytes_dummy(1_8:*)
integer(4),value::length_dummy
integer(4)::f_result
end
end interface
interface
function cdiinqkeybytes(cdiid_dummy,varid_dummy,key_dummy,bytes_dummy,length_dummy) bind(c,name="cdiInqKeyBytes") result(f_result)
integer(4),value::cdiid_dummy
integer(4),value::varid_dummy
integer(4),value::key_dummy
integer(1),intent(inout)::bytes_dummy(1_8:*)
integer(4),intent(inout)::length_dummy
integer(4)::f_result
end
end interface
interface
function cdiinqkeylen(cdiid_dummy,varid_dummy,key_dummy,length_dummy) bind(c,name="cdiInqKeyLen") result(f_result)
integer(4),value::cdiid_dummy
integer(4),value::varid_dummy
integer(4),value::key_dummy
integer(4),intent(inout)::length_dummy
integer(4)::f_result
end
end interface
interface
function cdicopykeys(cdiid1_dummy,varid1_dummy,cdiid2_dummy,varid2_dummy) bind(c,name="cdiCopyKeys") result(f_result)
integer(4),value::cdiid1_dummy
integer(4),value::varid1_dummy
integer(4),value::cdiid2_dummy
integer(4),value::varid2_dummy
integer(4)::f_result
end
end interface
interface
function cdicopykey(cdiid1_dummy,varid1_dummy,key_dummy,cdiid2_dummy) bind(c,name="cdiCopyKey") result(f_result)
integer(4),value::cdiid1_dummy
integer(4),value::varid1_dummy
integer(4),value::key_dummy
integer(4),value::cdiid2_dummy
integer(4)::f_result
end
end interface
interface
function cdideletekey(cdiid_dummy,varid_dummy,key_dummy) bind(c,name="cdiDeleteKey") result(f_result)
integer(4),value::cdiid_dummy
integer(4),value::varid_dummy
integer(4),value::key_dummy
integer(4)::f_result
end
end interface
interface
subroutine griddefdatatype(gridid_dummy,datatype_dummy) bind(c,name="gridDefDatatype")
integer(4),value::gridid_dummy
integer(4),value::datatype_dummy
end
end interface
interface
function gridinqdatatype(gridid_dummy) bind(c,name="gridInqDatatype") result(f_result)
integer(4),value::gridid_dummy
integer(4)::f_result
end
end interface
interface
function gridinqxval(gridid_dummy,index_dummy) bind(c,name="gridInqXval") result(f_result)
integer(4),value::gridid_dummy
integer(4),value::index_dummy
real(8)::f_result
end
end interface
interface
function gridinqyval(gridid_dummy,index_dummy) bind(c,name="gridInqYval") result(f_result)
integer(4),value::gridid_dummy
integer(4),value::index_dummy
real(8)::f_result
end
end interface
interface
function gridinqxinc(gridid_dummy) bind(c,name="gridInqXinc") result(f_result)
integer(4),value::gridid_dummy
real(8)::f_result
end
end interface
interface
function gridinqyinc(gridid_dummy) bind(c,name="gridInqYinc") result(f_result)
integer(4),value::gridid_dummy
real(8)::f_result
end
end interface
interface
function gridiscircular(gridid_dummy) bind(c,name="gridIsCircular") result(f_result)
integer(4),value::gridid_dummy
integer(4)::f_result
end
end interface
interface
function gridinqtrunc(gridid_dummy) bind(c,name="gridInqTrunc") result(f_result)
integer(4),value::gridid_dummy
integer(4)::f_result
end
end interface
interface
subroutine griddeftrunc(gridid_dummy,trunc_dummy) bind(c,name="gridDefTrunc")
integer(4),value::gridid_dummy
integer(4),value::trunc_dummy
end
end interface
interface
subroutine griddefnumber(gridid_dummy,number_dummy) bind(c,name="gridDefNumber")
integer(4),value::gridid_dummy
integer(4),value::number_dummy
end
end interface
interface
function gridinqnumber(gridid_dummy) bind(c,name="gridInqNumber") result(f_result)
integer(4),value::gridid_dummy
integer(4)::f_result
end
end interface
interface
subroutine griddefposition(gridid_dummy,position_dummy) bind(c,name="gridDefPosition")
integer(4),value::gridid_dummy
integer(4),value::position_dummy
end
end interface
interface
function gridinqposition(gridid_dummy) bind(c,name="gridInqPosition") result(f_result)
integer(4),value::gridid_dummy
integer(4)::f_result
end
end interface
interface
subroutine griddefuuid(gridid_dummy,uuid_dummy) bind(c,name="gridDefUUID")
integer(4),value::gridid_dummy
integer(1),intent(in)::uuid_dummy(1_8:16_8)
end
end interface
interface
subroutine gridinquuid(gridid_dummy,uuid_dummy) bind(c,name="gridInqUUID")
integer(4),value::gridid_dummy
integer(1),intent(inout)::uuid_dummy(1_8:16_8)
end
end interface
interface
subroutine griddefparamrll(gridid_dummy,xpole_dummy,ypole_dummy,angle_dummy) bind(c,name="gridDefParamRLL")
integer(4),value::gridid_dummy
real(8),value::xpole_dummy
real(8),value::ypole_dummy
real(8),value::angle_dummy
end
end interface
interface
subroutine gridinqparamrll(gridid_dummy,xpole_dummy,ypole_dummy,angle_dummy) bind(c,name="gridInqParamRLL")
integer(4),value::gridid_dummy
real(8),intent(inout)::xpole_dummy
real(8),intent(inout)::ypole_dummy
real(8),intent(inout)::angle_dummy
end
end interface
interface
subroutine griddefparamgme(gridid_dummy,nd_dummy,ni_dummy,ni2_dummy,ni3_dummy) bind(c,name="gridDefParamGME")
integer(4),value::gridid_dummy
integer(4),value::nd_dummy
integer(4),value::ni_dummy
integer(4),value::ni2_dummy
integer(4),value::ni3_dummy
end
end interface
interface
subroutine gridinqparamgme(gridid_dummy,nd_dummy,ni_dummy,ni2_dummy,ni3_dummy) bind(c,name="gridInqParamGME")
integer(4),value::gridid_dummy
integer(4),intent(inout)::nd_dummy
integer(4),intent(inout)::ni_dummy
integer(4),intent(inout)::ni2_dummy
integer(4),intent(inout)::ni3_dummy
end
end interface
interface
subroutine griddefarea(gridid_dummy,area_dummy) bind(c,name="gridDefArea")
integer(4),value::gridid_dummy
real(8),intent(in)::area_dummy(1_8:*)
end
end interface
interface
subroutine gridinqarea(gridid_dummy,area_dummy) bind(c,name="gridInqArea")
integer(4),value::gridid_dummy
real(8),intent(inout)::area_dummy(1_8:*)
end
end interface
interface
function gridhasarea(gridid_dummy) bind(c,name="gridHasArea") result(f_result)
integer(4),value::gridid_dummy
integer(4)::f_result
end
end interface
interface
subroutine griddefnvertex(gridid_dummy,nvertex_dummy) bind(c,name="gridDefNvertex")
integer(4),value::gridid_dummy
integer(4),value::nvertex_dummy
end
end interface
interface
function gridinqnvertex(gridid_dummy) bind(c,name="gridInqNvertex") result(f_result)
integer(4),value::gridid_dummy
integer(4)::f_result
end
end interface
interface
subroutine griddefxbounds(gridid_dummy,xbounds_dummy) bind(c,name="gridDefXbounds")
integer(4),value::gridid_dummy
real(8),intent(in)::xbounds_dummy(1_8:*)
end
end interface
interface
function gridinqxbounds(gridid_dummy,xbounds_dummy) bind(c,name="gridInqXbounds") result(f_result)
integer(4),value::gridid_dummy
real(8),intent(inout)::xbounds_dummy(1_8:*)
integer(4)::f_result
end
end interface
interface
function gridinqxboundspart(gridid_dummy,start_dummy,size_dummy,xbounds_dummy) bind(c,name="gridInqXboundsPart") result(f_result)
integer(4),value::gridid_dummy
integer(4),value::start_dummy
integer(4),value::size_dummy
real(8),intent(inout)::xbounds_dummy(1_8:*)
integer(4)::f_result
end
end interface
interface
function gridinqxboundsptr(gridid_dummy) bind(c,name="gridInqXboundsPtr") result(f_result)
import::c_ptr
integer(4),value::gridid_dummy
type(c_ptr)::f_result
end
end interface
interface
subroutine griddefybounds(gridid_dummy,ybounds_dummy) bind(c,name="gridDefYbounds")
integer(4),value::gridid_dummy
real(8),intent(in)::ybounds_dummy(1_8:*)
end
end interface
interface
function gridinqybounds(gridid_dummy,ybounds_dummy) bind(c,name="gridInqYbounds") result(f_result)
integer(4),value::gridid_dummy
real(8),intent(inout)::ybounds_dummy(1_8:*)
integer(4)::f_result
end
end interface
interface
function gridinqyboundspart(gridid_dummy,start_dummy,size_dummy,ybounds_dummy) bind(c,name="gridInqYboundsPart") result(f_result)
integer(4),value::gridid_dummy
integer(4),value::start_dummy
integer(4),value::size_dummy
real(8),intent(inout)::ybounds_dummy(1_8:*)
integer(4)::f_result
end
end interface
interface
function gridinqyboundsptr(gridid_dummy) bind(c,name="gridInqYboundsPtr") result(f_result)
import::c_ptr
integer(4),value::gridid_dummy
type(c_ptr)::f_result
end
end interface
interface
subroutine griddefreducedpoints(gridid_dummy,reducedpointssize_dummy,reducedpoints_dummy) bind(c,name="gridDefReducedPoints")
integer(4),value::gridid_dummy
integer(4),value::reducedpointssize_dummy
integer(4),intent(in)::reducedpoints_dummy(1_8:*)
end
end interface
interface
subroutine gridinqreducedpoints(gridid_dummy,reducedpoints_dummy) bind(c,name="gridInqReducedPoints")
integer(4),value::gridid_dummy
integer(4),intent(inout)::reducedpoints_dummy(1_8:*)
end
end interface
interface
subroutine gridchangetype(gridid_dummy,gridtype_dummy) bind(c,name="gridChangeType")
integer(4),value::gridid_dummy
integer(4),value::gridtype_dummy
end
end interface
interface
subroutine griddefcomplexpacking(gridid_dummy,lpack_dummy) bind(c,name="gridDefComplexPacking")
integer(4),value::gridid_dummy
integer(4),value::lpack_dummy
end
end interface
interface
function gridinqcomplexpacking(gridid_dummy) bind(c,name="gridInqComplexPacking") result(f_result)
integer(4),value::gridid_dummy
integer(4)::f_result
end
end interface
interface
function zaxiscreate(zaxistype_dummy,size_dummy) bind(c,name="zaxisCreate") result(f_result)
integer(4),value::zaxistype_dummy
integer(4),value::size_dummy
integer(4)::f_result
end
end interface
interface
subroutine zaxisdestroy(zaxisid_dummy) bind(c,name="zaxisDestroy")
integer(4),value::zaxisid_dummy
end
end interface
interface
function zaxisinqtype(zaxisid_dummy) bind(c,name="zaxisInqType") result(f_result)
integer(4),value::zaxisid_dummy
integer(4)::f_result
end
end interface
interface
function zaxisinqsize(zaxisid_dummy) bind(c,name="zaxisInqSize") result(f_result)
integer(4),value::zaxisid_dummy
integer(4)::f_result
end
end interface
interface
function zaxisduplicate(zaxisid_dummy) bind(c,name="zaxisDuplicate") result(f_result)
integer(4),value::zaxisid_dummy
integer(4)::f_result
end
end interface
interface
subroutine zaxisdeflevels(zaxisid_dummy,levels_dummy) bind(c,name="zaxisDefLevels")
integer(4),value::zaxisid_dummy
real(8),intent(in)::levels_dummy(1_8:*)
end
end interface
interface
function zaxisinqlevels(zaxisid_dummy,levels_dummy) bind(c,name="zaxisInqLevels") result(f_result)
integer(4),value::zaxisid_dummy
real(8),intent(inout)::levels_dummy(1_8:*)
integer(4)::f_result
end
end interface
interface
function zaxisinqclen(zaxisid_dummy) bind(c,name="zaxisInqCLen") result(f_result)
integer(4),value::zaxisid_dummy
integer(4)::f_result
end
end interface
interface
subroutine zaxisdeflevel(zaxisid_dummy,levelid_dummy,levels_dummy) bind(c,name="zaxisDefLevel")
integer(4),value::zaxisid_dummy
integer(4),value::levelid_dummy
real(8),value::levels_dummy
end
end interface
interface
function zaxisinqlevel(zaxisid_dummy,levelid_dummy) bind(c,name="zaxisInqLevel") result(f_result)
integer(4),value::zaxisid_dummy
integer(4),value::levelid_dummy
real(8)::f_result
end
end interface
interface
subroutine zaxisdefnlevref(gridid_dummy,nhlev_dummy) bind(c,name="zaxisDefNlevRef")
integer(4),value::gridid_dummy
integer(4),value::nhlev_dummy
end
end interface
interface
function zaxisinqnlevref(gridid_dummy) bind(c,name="zaxisInqNlevRef") result(f_result)
integer(4),value::gridid_dummy
integer(4)::f_result
end
end interface
interface
subroutine zaxisdefnumber(gridid_dummy,number_dummy) bind(c,name="zaxisDefNumber")
integer(4),value::gridid_dummy
integer(4),value::number_dummy
end
end interface
interface
function zaxisinqnumber(gridid_dummy) bind(c,name="zaxisInqNumber") result(f_result)
integer(4),value::gridid_dummy
integer(4)::f_result
end
end interface
interface
subroutine zaxisdefuuid(zaxisid_dummy,uuid_dummy) bind(c,name="zaxisDefUUID")
integer(4),value::zaxisid_dummy
integer(1),intent(in)::uuid_dummy(1_8:16_8)
end
end interface
interface
subroutine zaxisinquuid(zaxisid_dummy,uuid_dummy) bind(c,name="zaxisInqUUID")
integer(4),value::zaxisid_dummy
integer(1),intent(inout)::uuid_dummy(1_8:16_8)
end
end interface
interface
subroutine zaxisdefdatatype(zaxisid_dummy,datatype_dummy) bind(c,name="zaxisDefDatatype")
integer(4),value::zaxisid_dummy
integer(4),value::datatype_dummy
end
end interface
interface
function zaxisinqdatatype(zaxisid_dummy) bind(c,name="zaxisInqDatatype") result(f_result)
integer(4),value::zaxisid_dummy
integer(4)::f_result
end
end interface
interface
subroutine zaxisdefpositive(zaxisid_dummy,positive_dummy) bind(c,name="zaxisDefPositive")
integer(4),value::zaxisid_dummy
integer(4),value::positive_dummy
end
end interface
interface
function zaxisinqpositive(zaxisid_dummy) bind(c,name="zaxisInqPositive") result(f_result)
integer(4),value::zaxisid_dummy
integer(4)::f_result
end
end interface
interface
subroutine zaxisdefscalar(zaxisid_dummy) bind(c,name="zaxisDefScalar")
integer(4),value::zaxisid_dummy
end
end interface
interface
function zaxisinqscalar(zaxisid_dummy) bind(c,name="zaxisInqScalar") result(f_result)
integer(4),value::zaxisid_dummy
integer(4)::f_result
end
end interface
interface
subroutine zaxisdefvct(zaxisid_dummy,size_dummy,vct_dummy) bind(c,name="zaxisDefVct")
integer(4),value::zaxisid_dummy
integer(4),value::size_dummy
real(8),intent(in)::vct_dummy(1_8:*)
end
end interface
interface
subroutine zaxisinqvct(zaxisid_dummy,vct_dummy) bind(c,name="zaxisInqVct")
integer(4),value::zaxisid_dummy
real(8),intent(inout)::vct_dummy(1_8:*)
end
end interface
interface
function zaxisinqvctsize(zaxisid_dummy) bind(c,name="zaxisInqVctSize") result(f_result)
integer(4),value::zaxisid_dummy
integer(4)::f_result
end
end interface
interface
function zaxisinqvctptr(zaxisid_dummy) bind(c,name="zaxisInqVctPtr") result(f_result)
import::c_ptr
integer(4),value::zaxisid_dummy
type(c_ptr)::f_result
end
end interface
interface
subroutine zaxisdeflbounds(zaxisid_dummy,lbounds_dummy) bind(c,name="zaxisDefLbounds")
integer(4),value::zaxisid_dummy
real(8),intent(in)::lbounds_dummy(1_8:*)
end
end interface
interface
function zaxisinqlbound(zaxisid_dummy,index_dummy) bind(c,name="zaxisInqLbound") result(f_result)
integer(4),value::zaxisid_dummy
integer(4),value::index_dummy
real(8)::f_result
end
end interface
interface
subroutine zaxisdefubounds(zaxisid_dummy,ubounds_dummy) bind(c,name="zaxisDefUbounds")
integer(4),value::zaxisid_dummy
real(8),intent(in)::ubounds_dummy(1_8:*)
end
end interface
interface
function zaxisinqubound(zaxisid_dummy,index_dummy) bind(c,name="zaxisInqUbound") result(f_result)
integer(4),value::zaxisid_dummy
integer(4),value::index_dummy
real(8)::f_result
end
end interface
interface
subroutine zaxisdefweights(zaxisid_dummy,weights_dummy) bind(c,name="zaxisDefWeights")
integer(4),value::zaxisid_dummy
real(8),intent(in)::weights_dummy(1_8:*)
end
end interface
interface
subroutine zaxischangetype(zaxisid_dummy,zaxistype_dummy) bind(c,name="zaxisChangeType")
integer(4),value::zaxisid_dummy
integer(4),value::zaxistype_dummy
end
end interface
interface
function taxiscreate(taxistype_dummy) bind(c,name="taxisCreate") result(f_result)
integer(4),value::taxistype_dummy
integer(4)::f_result
end
end interface
interface
subroutine taxisdestroy(taxisid_dummy) bind(c,name="taxisDestroy")
integer(4),value::taxisid_dummy
end
end interface
interface
function taxisduplicate(taxisid_dummy) bind(c,name="taxisDuplicate") result(f_result)
integer(4),value::taxisid_dummy
integer(4)::f_result
end
end interface
interface
subroutine taxiscopytimestep(taxisiddes_dummy,taxisidsrc_dummy) bind(c,name="taxisCopyTimestep")
integer(4),value::taxisiddes_dummy
integer(4),value::taxisidsrc_dummy
end
end interface
interface
subroutine taxisdeftype(taxisid_dummy,taxistype_dummy) bind(c,name="taxisDefType")
integer(4),value::taxisid_dummy
integer(4),value::taxistype_dummy
end
end interface
interface
function taxisinqtype(taxisid_dummy) bind(c,name="taxisInqType") result(f_result)
integer(4),value::taxisid_dummy
integer(4)::f_result
end
end interface
interface
subroutine taxisdefvdate(taxisid_dummy,date_dummy) bind(c,name="taxisDefVdate")
integer(4),value::taxisid_dummy
integer(4),value::date_dummy
end
end interface
interface
subroutine taxisdefvtime(taxisid_dummy,time_dummy) bind(c,name="taxisDefVtime")
integer(4),value::taxisid_dummy
integer(4),value::time_dummy
end
end interface
interface
function taxisinqvdate(taxisid_dummy) bind(c,name="taxisInqVdate") result(f_result)
integer(4),value::taxisid_dummy
integer(4)::f_result
end
end interface
interface
function taxisinqvtime(taxisid_dummy) bind(c,name="taxisInqVtime") result(f_result)
integer(4),value::taxisid_dummy
integer(4)::f_result
end
end interface
interface
subroutine taxisdefrdate(taxisid_dummy,date_dummy) bind(c,name="taxisDefRdate")
integer(4),value::taxisid_dummy
integer(4),value::date_dummy
end
end interface
interface
subroutine taxisdefrtime(taxisid_dummy,time_dummy) bind(c,name="taxisDefRtime")
integer(4),value::taxisid_dummy
integer(4),value::time_dummy
end
end interface
interface
function taxisinqrdate(taxisid_dummy) bind(c,name="taxisInqRdate") result(f_result)
integer(4),value::taxisid_dummy
integer(4)::f_result
end
end interface
interface
function taxisinqrtime(taxisid_dummy) bind(c,name="taxisInqRtime") result(f_result)
integer(4),value::taxisid_dummy
integer(4)::f_result
end
end interface
interface
function taxishasbounds(taxisid_dummy) bind(c,name="taxisHasBounds") result(f_result)
integer(4),value::taxisid_dummy
integer(4)::f_result
end
end interface
interface
subroutine taxiswithbounds(taxisid_dummy) bind(c,name="taxisWithBounds")
integer(4),value::taxisid_dummy
end
end interface
interface
subroutine taxisdeletebounds(taxisid_dummy) bind(c,name="taxisDeleteBounds")
integer(4),value::taxisid_dummy
end
end interface
interface
subroutine taxisdefvdatebounds(taxisid_dummy,vdate_lb_dummy,vdate_ub_dummy) bind(c,name="taxisDefVdateBounds")
integer(4),value::taxisid_dummy
integer(4),value::vdate_lb_dummy
integer(4),value::vdate_ub_dummy
end
end interface
interface
subroutine taxisdefvtimebounds(taxisid_dummy,vtime_lb_dummy,vtime_ub_dummy) bind(c,name="taxisDefVtimeBounds")
integer(4),value::taxisid_dummy
integer(4),value::vtime_lb_dummy
integer(4),value::vtime_ub_dummy
end
end interface
interface
subroutine taxisinqvdatebounds(taxisid_dummy,vdate_lb_dummy,vdate_ub_dummy) bind(c,name="taxisInqVdateBounds")
integer(4),value::taxisid_dummy
integer(4),intent(inout)::vdate_lb_dummy
integer(4),intent(inout)::vdate_ub_dummy
end
end interface
interface
subroutine taxisinqvtimebounds(taxisid_dummy,vtime_lb_dummy,vtime_ub_dummy) bind(c,name="taxisInqVtimeBounds")
integer(4),value::taxisid_dummy
integer(4),intent(inout)::vtime_lb_dummy
integer(4),intent(inout)::vtime_ub_dummy
end
end interface
interface
subroutine taxisdefcalendar(taxisid_dummy,calendar_dummy) bind(c,name="taxisDefCalendar")
integer(4),value::taxisid_dummy
integer(4),value::calendar_dummy
end
end interface
interface
function taxisinqcalendar(taxisid_dummy) bind(c,name="taxisInqCalendar") result(f_result)
integer(4),value::taxisid_dummy
integer(4)::f_result
end
end interface
interface
subroutine taxisdeftunit(taxisid_dummy,tunit_dummy) bind(c,name="taxisDefTunit")
integer(4),value::taxisid_dummy
integer(4),value::tunit_dummy
end
end interface
interface
function taxisinqtunit(taxisid_dummy) bind(c,name="taxisInqTunit") result(f_result)
integer(4),value::taxisid_dummy
integer(4)::f_result
end
end interface
interface
subroutine taxisdefforecasttunit(taxisid_dummy,tunit_dummy) bind(c,name="taxisDefForecastTunit")
integer(4),value::taxisid_dummy
integer(4),value::tunit_dummy
end
end interface
interface
function taxisinqforecasttunit(taxisid_dummy) bind(c,name="taxisInqForecastTunit") result(f_result)
integer(4),value::taxisid_dummy
integer(4)::f_result
end
end interface
interface
subroutine taxisdefforecastperiod(taxisid_dummy,fc_period_dummy) bind(c,name="taxisDefForecastPeriod")
integer(4),value::taxisid_dummy
real(8),value::fc_period_dummy
end
end interface
interface
function taxisinqforecastperiod(taxisid_dummy) bind(c,name="taxisInqForecastPeriod") result(f_result)
integer(4),value::taxisid_dummy
real(8)::f_result
end
end interface
interface
subroutine taxisdefnumavg(taxisid_dummy,numavg_dummy) bind(c,name="taxisDefNumavg")
integer(4),value::taxisid_dummy
integer(4),value::numavg_dummy
end
end interface
interface
function taxisinqnumavg(taxisid_dummy) bind(c,name="taxisInqNumavg") result(f_result)
integer(4),value::taxisid_dummy
integer(4)::f_result
end
end interface
interface
function institutinqnumber() bind(c,name="institutInqNumber") result(f_result)
integer(4)::f_result
end
end interface
interface
function institutinqcenter(instid_dummy) bind(c,name="institutInqCenter") result(f_result)
integer(4),value::instid_dummy
integer(4)::f_result
end
end interface
interface
function institutinqsubcenter(instid_dummy) bind(c,name="institutInqSubcenter") result(f_result)
integer(4),value::instid_dummy
integer(4)::f_result
end
end interface
interface
function modelinqinstitut(modelid_dummy) bind(c,name="modelInqInstitut") result(f_result)
integer(4),value::modelid_dummy
integer(4)::f_result
end
end interface
interface
function modelinqgribid(modelid_dummy) bind(c,name="modelInqGribID") result(f_result)
integer(4),value::modelid_dummy
integer(4)::f_result
end
end interface
interface
function tableinqnumber() bind(c,name="tableInqNumber") result(f_result)
integer(4)::f_result
end
end interface
interface
function tableinqnum(tableid_dummy) bind(c,name="tableInqNum") result(f_result)
integer(4),value::tableid_dummy
integer(4)::f_result
end
end interface
interface
function tableinqmodel(tableid_dummy) bind(c,name="tableInqModel") result(f_result)
integer(4),value::tableid_dummy
integer(4)::f_result
end
end interface
interface
function subtypecreate(subtype_dummy) bind(c,name="subtypeCreate") result(f_result)
integer(4),value::subtype_dummy
integer(4)::f_result
end
end interface
interface
subroutine subtypeprint(subtypeid_dummy) bind(c,name="subtypePrint")
integer(4),value::subtypeid_dummy
end
end interface
interface
function subtypecompare(subtypeid1_dummy,subtypeid2_dummy) bind(c,name="subtypeCompare") result(f_result)
integer(4),value::subtypeid1_dummy
integer(4),value::subtypeid2_dummy
integer(4)::f_result
end
end interface
interface
function subtypeinqsize(subtypeid_dummy) bind(c,name="subtypeInqSize") result(f_result)
integer(4),value::subtypeid_dummy
integer(4)::f_result
end
end interface
interface
function subtypeinqactiveindex(subtypeid_dummy) bind(c,name="subtypeInqActiveIndex") result(f_result)
integer(4),value::subtypeid_dummy
integer(4)::f_result
end
end interface
interface
subroutine subtypedefactiveindex(subtypeid_dummy,index_dummy) bind(c,name="subtypeDefActiveIndex")
integer(4),value::subtypeid_dummy
integer(4),value::index_dummy
end
end interface
interface
function subtypeinqtile(subtypeid_dummy,tileindex_dummy,attribute_dummy) bind(c,name="subtypeInqTile") result(f_result)
integer(4),value::subtypeid_dummy
integer(4),value::tileindex_dummy
integer(4),value::attribute_dummy
integer(4)::f_result
end
end interface
interface
function vlistinqvarsubtype(vlistid_dummy,varid_dummy) bind(c,name="vlistInqVarSubtype") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4)::f_result
end
end interface
interface
subroutine gribapilibraryversion(major_version_dummy,minor_version_dummy,revision_version_dummy) bind(c,name="gribapiLibraryVersion")
integer(4),intent(inout)::major_version_dummy
integer(4),intent(inout)::minor_version_dummy
integer(4),intent(inout)::revision_version_dummy
end
end interface
interface
subroutine zaxisdefltype(zaxisid_dummy,ltype_dummy) bind(c,name="zaxisDefLtype")
integer(4),value::zaxisid_dummy
integer(4),value::ltype_dummy
end
end interface
interface
function vlistinqvartypeofgeneratingprocess(vlistid_dummy,varid_dummy) bind(c,name="vlistInqVarTypeOfGeneratingProcess") result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4)::f_result
end
end interface
interface
subroutine vlistdefvartypeofgeneratingprocess(vlistid_dummy,varid_dummy,typeofgeneratingprocess_dummy) bind(c,name="vlistDefVarTypeOfGeneratingProcess")
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4),value::typeofgeneratingprocess_dummy
end
end interface
interface
subroutine vlistdefvarproductdefinitiontemplate(vlistid_dummy,varid_dummy,productdefinitiontemplate_dummy) bind(c,name="vlistDefVarProductDefinitionTemplate")
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
integer(4),value::productdefinitiontemplate_dummy
end
end interface
interface
function date_to_julday(calendar_dummy,date_dummy) bind(c,name="date_to_julday") result(f_result)
integer(4),value::calendar_dummy
integer(8),value::date_dummy
integer(8)::f_result
end
end interface
interface
function julday_to_date(calendar_dummy,julday_dummy) bind(c,name="julday_to_date") result(f_result)
integer(4),value::calendar_dummy
integer(8),value::julday_dummy
integer(8)::f_result
end
end interface
interface
function time_to_sec(time_dummy) bind(c,name="time_to_sec") result(f_result)
integer(4),value::time_dummy
integer(4)::f_result
end
end interface
interface
function sec_to_time(secofday_dummy) bind(c,name="sec_to_time") result(f_result)
integer(4),value::secofday_dummy
integer(4)::f_result
end
end interface
contains
subroutine ctrim(str)
character(*,1),intent(inout)::str
end
function c_len(s) result(i)
character(*,1),intent(in)::s
integer(4)::i
end
function cdistringerror(cdierrno_dummy) result(f_result)
integer(4),value::cdierrno_dummy
character(1_8,1),pointer::f_result(:)
end
function cdilibraryversion() result(f_result)
character(1_8,1),pointer::f_result(:)
end
subroutine cdidefglobal(string_dummy,val_dummy)
character(*,1),intent(in)::string_dummy
integer(4),value::val_dummy
end
subroutine cdiparamtostring(param_dummy,paramstr_dummy,maxlen_dummy)
integer(4),value::param_dummy
character(*,1),intent(inout)::paramstr_dummy
integer(4),value::maxlen_dummy
end
function cdigetfiletype(uri_dummy,byteorder_dummy) result(f_result)
character(*,1),intent(in)::uri_dummy
integer(4),intent(inout)::byteorder_dummy
integer(4)::f_result
end
function streamopenread(path_dummy) result(f_result)
character(*,1),intent(in)::path_dummy
integer(4)::f_result
end
function streamopenwrite(path_dummy,filetype_dummy) result(f_result)
character(*,1),intent(in)::path_dummy
integer(4),value::filetype_dummy
integer(4)::f_result
end
function streamopenappend(path_dummy) result(f_result)
character(*,1),intent(in)::path_dummy
integer(4)::f_result
end
function streamfilename(streamid_dummy) result(f_result)
integer(4),value::streamid_dummy
character(1_8,1),pointer::f_result(:)
end
function streamfilesuffix(filetype_dummy) result(f_result)
integer(4),value::filetype_dummy
character(1_8,1),pointer::f_result(:)
end
function cdiiterator_new(path_dummy) result(f_result)
character(*,1),intent(in)::path_dummy
type(t_cdiiterator)::f_result
end
function cdiiterator_clone(me_dummy) result(f_result)
type(t_cdiiterator),intent(in)::me_dummy
type(t_cdiiterator)::f_result
end
function cdiiterator_serialize(me_dummy) result(f_result)
type(t_cdiiterator),intent(in)::me_dummy
character(1_8,1),pointer::f_result(:)
end
function cdiiterator_deserialize(description_dummy) result(f_result)
character(*,1),intent(in)::description_dummy
type(t_cdiiterator)::f_result
end
subroutine cdiiterator_delete(me_dummy)
type(t_cdiiterator),intent(in)::me_dummy
end
function cdiiterator_nextfield(me_dummy) result(f_result)
type(t_cdiiterator),intent(in)::me_dummy
integer(4)::f_result
end
function cdiiterator_inqstarttime(me_dummy) result(f_result)
type(t_cdiiterator),intent(in)::me_dummy
character(1_8,1),pointer::f_result(:)
end
function cdiiterator_inqendtime(me_dummy) result(f_result)
type(t_cdiiterator),intent(in)::me_dummy
character(1_8,1),pointer::f_result(:)
end
function cdiiterator_inqrtime(me_dummy) result(f_result)
type(t_cdiiterator),intent(in)::me_dummy
character(1_8,1),pointer::f_result(:)
end
function cdiiterator_inqvtime(me_dummy) result(f_result)
type(t_cdiiterator),intent(in)::me_dummy
character(1_8,1),pointer::f_result(:)
end
function cdiiterator_inqleveltype(me_dummy,levelselector_dummy,outname,outlongname,outstdname,outunit) result(f_result)
type(t_cdiiterator),intent(in)::me_dummy
integer(4),value::levelselector_dummy
character(1_8,1),intent(inout),optional,pointer::outname(:)
character(1_8,1),intent(inout),optional,pointer::outlongname(:)
character(1_8,1),intent(inout),optional,pointer::outstdname(:)
character(1_8,1),intent(inout),optional,pointer::outunit(:)
integer(4)::f_result
end
function cdiiterator_inqlevel(me_dummy,levelselector_dummy,outvalue1,outvalue2) result(f_result)
type(t_cdiiterator),intent(in)::me_dummy
integer(4),value::levelselector_dummy
real(8),intent(inout),optional,target::outvalue1
real(8),intent(inout),optional,target::outvalue2
integer(4)::f_result
end
function cdiiterator_inqleveluuid(me_dummy,outvgridnumber,outlevelcount,outuuid) result(f_result)
type(t_cdiiterator),intent(in)::me_dummy
integer(4),intent(inout),optional,target::outvgridnumber
integer(4),intent(inout),optional,target::outlevelcount
integer(1),intent(inout),optional,target::outuuid(1_8:16_8)
integer(4)::f_result
end
function cdiiterator_inqtile(me_dummy,outtileindex_dummy,outtileattribute_dummy) result(f_result)
type(t_cdiiterator),intent(in)::me_dummy
integer(4),intent(inout)::outtileindex_dummy
integer(4),intent(inout)::outtileattribute_dummy
integer(4)::f_result
end
function cdiiterator_inqtilecount(me_dummy,outtilecount_dummy,outtileattributecount_dummy) result(f_result)
type(t_cdiiterator),intent(in)::me_dummy
integer(4),intent(inout)::outtilecount_dummy
integer(4),intent(inout)::outtileattributecount_dummy
integer(4)::f_result
end
function cdiiterator_inqparam(me_dummy) result(f_result)
type(t_cdiiterator),intent(in)::me_dummy
type(t_cdiparam)::f_result
end
subroutine cdiiterator_inqparamparts(me_dummy,outdiscipline_dummy,outcategory_dummy,outnumber_dummy)
type(t_cdiiterator),intent(in)::me_dummy
integer(4),intent(inout)::outdiscipline_dummy
integer(4),intent(inout)::outcategory_dummy
integer(4),intent(inout)::outnumber_dummy
end
function cdiiterator_inqdatatype(me_dummy) result(f_result)
type(t_cdiiterator),intent(in)::me_dummy
integer(4)::f_result
end
function cdiiterator_inqfiletype(me_dummy) result(f_result)
type(t_cdiiterator),intent(in)::me_dummy
integer(4)::f_result
end
function cdiiterator_inqtsteptype(me_dummy) result(f_result)
type(t_cdiiterator),intent(in)::me_dummy
integer(4)::f_result
end
function cdiiterator_inqvariablename(me_dummy) result(f_result)
type(t_cdiiterator),intent(in)::me_dummy
character(1_8,1),pointer::f_result(:)
end
function cdiiterator_inqgridid(me_dummy) result(f_result)
type(t_cdiiterator),intent(in)::me_dummy
integer(4)::f_result
end
subroutine cdiiterator_readfield(me_dummy,data_dummy,nmiss)
type(t_cdiiterator),intent(in)::me_dummy
real(8),intent(inout)::data_dummy(1_8:*)
integer(4),intent(inout),optional,target::nmiss
end
subroutine cdiiterator_readfieldf(me_dummy,data_dummy,nmiss)
type(t_cdiiterator),intent(in)::me_dummy
real(4),intent(inout)::data_dummy(1_8:*)
integer(4),intent(inout),optional,target::nmiss
end
function cdigribiterator_clone(me_dummy) result(f_result)
type(t_cdiiterator),intent(in)::me_dummy
type(t_cdigribiterator)::f_result
end
subroutine cdigribiterator_delete(me_dummy)
type(t_cdigribiterator),intent(in)::me_dummy
end
function cdigribiterator_getlong(me_dummy,key_dummy,value_dummy) result(f_result)
type(t_cdigribiterator),intent(in)::me_dummy
character(*,1),intent(in)::key_dummy
integer(8),intent(inout)::value_dummy
integer(4)::f_result
end
function cdigribiterator_getdouble(me_dummy,key_dummy,value_dummy) result(f_result)
type(t_cdigribiterator),intent(in)::me_dummy
character(*,1),intent(in)::key_dummy
real(8),intent(inout)::value_dummy
integer(4)::f_result
end
function cdigribiterator_getlength(me_dummy,key_dummy,value_dummy) result(f_result)
type(t_cdigribiterator),intent(in)::me_dummy
character(*,1),intent(in)::key_dummy
integer(8),intent(inout)::value_dummy
integer(4)::f_result
end
function cdigribiterator_getstring(me_dummy,key_dummy,value_dummy,length_dummy) result(f_result)
type(t_cdigribiterator),intent(in)::me_dummy
character(*,1),intent(in)::key_dummy
character(*,1),intent(inout)::value_dummy
integer(8),intent(inout)::length_dummy
integer(4)::f_result
end
function cdigribiterator_getsize(me_dummy,key_dummy,value_dummy) result(f_result)
type(t_cdigribiterator),intent(in)::me_dummy
character(*,1),intent(in)::key_dummy
integer(8),intent(inout)::value_dummy
integer(4)::f_result
end
function cdigribiterator_getlongarray(me_dummy,key_dummy,value_dummy,array_size_dummy) result(f_result)
type(t_cdigribiterator),intent(in)::me_dummy
character(*,1),intent(in)::key_dummy
integer(8),intent(inout)::value_dummy
integer(8),intent(inout)::array_size_dummy
integer(4)::f_result
end
function cdigribiterator_getdoublearray(me_dummy,key_dummy,value_dummy,array_size_dummy) result(f_result)
type(t_cdigribiterator),intent(in)::me_dummy
character(*,1),intent(in)::key_dummy
real(8),intent(inout)::value_dummy
integer(8),intent(inout)::array_size_dummy
integer(4)::f_result
end
function cdigribiterator_inqedition(me_dummy) result(f_result)
type(t_cdigribiterator),intent(in)::me_dummy
integer(4)::f_result
end
function cdigribiterator_inqlongvalue(me_dummy,key_dummy) result(f_result)
type(t_cdigribiterator),intent(in)::me_dummy
character(*,1),intent(in)::key_dummy
integer(8)::f_result
end
function cdigribiterator_inqlongdefaultvalue(me_dummy,key_dummy,defaultvalue_dummy) result(f_result)
type(t_cdigribiterator),intent(in)::me_dummy
character(*,1),intent(in)::key_dummy
integer(8),value::defaultvalue_dummy
integer(8)::f_result
end
function cdigribiterator_inqdoublevalue(me_dummy,key_dummy) result(f_result)
type(t_cdigribiterator),intent(in)::me_dummy
character(*,1),intent(in)::key_dummy
real(8)::f_result
end
function cdigribiterator_inqdoubledefaultvalue(me_dummy,key_dummy,defaultvalue_dummy) result(f_result)
type(t_cdigribiterator),intent(in)::me_dummy
character(*,1),intent(in)::key_dummy
real(8),value::defaultvalue_dummy
real(8)::f_result
end
function cdigribiterator_inqstringvalue(me_dummy,key_dummy) result(f_result)
type(t_cdigribiterator),intent(in)::me_dummy
character(*,1),intent(in)::key_dummy
character(1_8,1),pointer::f_result(:)
end
subroutine vlistdefvarname(vlistid_dummy,varid_dummy,name_dummy)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
character(*,1),intent(in)::name_dummy
end
subroutine vlistinqvarname(vlistid_dummy,varid_dummy,name_dummy)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
character(*,1),intent(inout)::name_dummy
end
function vlistcopyvarname(vlistid_dummy,varid_dummy) result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
character(1_8,1),pointer::f_result(:)
end
subroutine vlistdefvarstdname(vlistid_dummy,varid_dummy,stdname_dummy)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
character(*,1),intent(in)::stdname_dummy
end
subroutine vlistinqvarstdname(vlistid_dummy,varid_dummy,stdname_dummy)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
character(*,1),intent(inout)::stdname_dummy
end
subroutine vlistdefvarlongname(vlistid_dummy,varid_dummy,longname_dummy)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
character(*,1),intent(in)::longname_dummy
end
subroutine vlistinqvarlongname(vlistid_dummy,varid_dummy,longname_dummy)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
character(*,1),intent(inout)::longname_dummy
end
subroutine vlistdefvarunits(vlistid_dummy,varid_dummy,units_dummy)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
character(*,1),intent(in)::units_dummy
end
subroutine vlistinqvarunits(vlistid_dummy,varid_dummy,units_dummy)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
character(*,1),intent(inout)::units_dummy
end
subroutine cdidefadditionalkey(string_dummy)
character(*,1),intent(in)::string_dummy
end
subroutine vlistdefvarintkey(vlistid_dummy,varid_dummy,name_dummy,value_dummy)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
character(*,1),intent(in)::name_dummy
integer(4),value::value_dummy
end
subroutine vlistdefvardblkey(vlistid_dummy,varid_dummy,name_dummy,value_dummy)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
character(*,1),intent(in)::name_dummy
real(8),value::value_dummy
end
function vlisthasvarkey(vlistid_dummy,varid_dummy,name_dummy) result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
character(*,1),intent(in)::name_dummy
integer(4)::f_result
end
function vlistinqvardblkey(vlistid_dummy,varid_dummy,name_dummy) result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
character(*,1),intent(in)::name_dummy
real(8)::f_result
end
function vlistinqvarintkey(vlistid_dummy,varid_dummy,name_dummy) result(f_result)
integer(4),value::vlistid_dummy
integer(4),value::varid_dummy
character(*,1),intent(in)::name_dummy
integer(4)::f_result
end
function cdiinqatt(cdiid_dummy,varid_dummy,attrnum_dummy,name_dummy,typep_dummy,lenp_dummy) result(f_result)
integer(4),value::cdiid_dummy
integer(4),value::varid_dummy
integer(4),value::attrnum_dummy
character(*,1),intent(inout)::name_dummy
integer(4),intent(inout)::typep_dummy
integer(4),intent(inout)::lenp_dummy
integer(4)::f_result
end
function cdiinqattlen(cdiid_dummy,varid_dummy,name_dummy) result(f_result)
integer(4),value::cdiid_dummy
integer(4),value::varid_dummy
character(*,1),intent(in)::name_dummy
integer(4)::f_result
end
function cdiinqatttype(cdiid_dummy,varid_dummy,name_dummy) result(f_result)
integer(4),value::cdiid_dummy
integer(4),value::varid_dummy
character(*,1),intent(in)::name_dummy
integer(4)::f_result
end
function cdidelatt(cdiid_dummy,varid_dummy,name_dummy) result(f_result)
integer(4),value::cdiid_dummy
integer(4),value::varid_dummy
character(*,1),intent(in)::name_dummy
integer(4)::f_result
end
function cdidefattint(cdiid_dummy,varid_dummy,name_dummy,type_dummy,len_dummy,ip_dummy) result(f_result)
integer(4),value::cdiid_dummy
integer(4),value::varid_dummy
character(*,1),intent(in)::name_dummy
integer(4),value::type_dummy
integer(4),value::len_dummy
integer(4),intent(in)::ip_dummy(1_8:*)
integer(4)::f_result
end
function cdidefattflt(cdiid_dummy,varid_dummy,name_dummy,type_dummy,len_dummy,dp_dummy) result(f_result)
integer(4),value::cdiid_dummy
integer(4),value::varid_dummy
character(*,1),intent(in)::name_dummy
integer(4),value::type_dummy
integer(4),value::len_dummy
real(8),intent(in)::dp_dummy(1_8:*)
integer(4)::f_result
end
function cdidefatttxt(cdiid_dummy,varid_dummy,name_dummy,len_dummy,tp_cbuf_dummy) result(f_result)
integer(4),value::cdiid_dummy
integer(4),value::varid_dummy
character(*,1),intent(in)::name_dummy
integer(4),value::len_dummy
character(*,1),intent(in)::tp_cbuf_dummy
integer(4)::f_result
end
function cdiinqattint(cdiid_dummy,varid_dummy,name_dummy,mlen_dummy,ip_dummy) result(f_result)
integer(4),value::cdiid_dummy
integer(4),value::varid_dummy
character(*,1),intent(in)::name_dummy
integer(4),value::mlen_dummy
integer(4),intent(inout)::ip_dummy(1_8:*)
integer(4)::f_result
end
function cdiinqattflt(cdiid_dummy,varid_dummy,name_dummy,mlen_dummy,dp_dummy) result(f_result)
integer(4),value::cdiid_dummy
integer(4),value::varid_dummy
character(*,1),intent(in)::name_dummy
integer(4),value::mlen_dummy
real(8),intent(inout)::dp_dummy(1_8:*)
integer(4)::f_result
end
function cdiinqatttxt(cdiid_dummy,varid_dummy,name_dummy,mlen_dummy,tp_cbuf_dummy) result(f_result)
integer(4),value::cdiid_dummy
integer(4),value::varid_dummy
character(*,1),intent(in)::name_dummy
integer(4),value::mlen_dummy
character(*,1),intent(inout)::tp_cbuf_dummy
integer(4)::f_result
end
subroutine gridname(gridtype_dummy,gridname_dummy)
integer(4),value::gridtype_dummy
character(*,1),intent(inout)::gridname_dummy
end
function gridnameptr(gridtype_dummy) result(f_result)
integer(4),value::gridtype_dummy
character(1_8,1),pointer::f_result(:)
end
function cdidefkeystring(cdiid_dummy,varid_dummy,key_dummy,string_dummy) result(f_result)
integer(4),value::cdiid_dummy
integer(4),value::varid_dummy
integer(4),value::key_dummy
character(*,1),intent(in)::string_dummy
integer(4)::f_result
end
function cdiinqkeystring(cdiid_dummy,varid_dummy,key_dummy,string_dummy,length_dummy) result(f_result)
integer(4),value::cdiid_dummy
integer(4),value::varid_dummy
integer(4),value::key_dummy
character(*,1),intent(inout)::string_dummy
integer(4),intent(inout)::length_dummy
integer(4)::f_result
end
subroutine griddefxname(gridid_dummy,xname_dummy)
integer(4),value::gridid_dummy
character(*,1),intent(in)::xname_dummy
end
subroutine gridinqxname(gridid_dummy,xname_dummy)
integer(4),value::gridid_dummy
character(*,1),intent(inout)::xname_dummy
end
subroutine griddefxlongname(gridid_dummy,xlongname_dummy)
integer(4),value::gridid_dummy
character(*,1),intent(in)::xlongname_dummy
end
subroutine gridinqxlongname(gridid_dummy,xlongname_dummy)
integer(4),value::gridid_dummy
character(*,1),intent(inout)::xlongname_dummy
end
subroutine griddefxunits(gridid_dummy,xunits_dummy)
integer(4),value::gridid_dummy
character(*,1),intent(in)::xunits_dummy
end
subroutine gridinqxunits(gridid_dummy,xunits_dummy)
integer(4),value::gridid_dummy
character(*,1),intent(inout)::xunits_dummy
end
subroutine griddefyname(gridid_dummy,yname_dummy)
integer(4),value::gridid_dummy
character(*,1),intent(in)::yname_dummy
end
subroutine gridinqyname(gridid_dummy,yname_dummy)
integer(4),value::gridid_dummy
character(*,1),intent(inout)::yname_dummy
end
subroutine griddefylongname(gridid_dummy,ylongname_dummy)
integer(4),value::gridid_dummy
character(*,1),intent(in)::ylongname_dummy
end
subroutine gridinqylongname(gridid_dummy,ylongname_dummy)
integer(4),value::gridid_dummy
character(*,1),intent(inout)::ylongname_dummy
end
subroutine griddefyunits(gridid_dummy,yunits_dummy)
integer(4),value::gridid_dummy
character(*,1),intent(in)::yunits_dummy
end
subroutine gridinqyunits(gridid_dummy,yunits_dummy)
integer(4),value::gridid_dummy
character(*,1),intent(inout)::yunits_dummy
end
subroutine griddefreference(gridid_dummy,reference_dummy)
integer(4),value::gridid_dummy
character(*,1),intent(in)::reference_dummy
end
function gridinqreference(gridid_dummy,reference_dummy) result(f_result)
integer(4),value::gridid_dummy
character(*,1),intent(inout)::reference_dummy
integer(4)::f_result
end
subroutine zaxisname(zaxistype_dummy,zaxisname_dummy)
integer(4),value::zaxistype_dummy
character(*,1),intent(inout)::zaxisname_dummy
end
function zaxisnameptr(leveltype_dummy) result(f_result)
integer(4),value::leveltype_dummy
character(1_8,1),pointer::f_result(:)
end
subroutine zaxisdefname(zaxisid_dummy,name)
integer(4),value::zaxisid_dummy
character(*,1),intent(in),optional::name
end
subroutine zaxisinqname(zaxisid_dummy,name_dummy)
integer(4),value::zaxisid_dummy
character(*,1),intent(inout)::name_dummy
end
subroutine zaxisdeflongname(zaxisid_dummy,longname)
integer(4),value::zaxisid_dummy
character(*,1),intent(in),optional::longname
end
subroutine zaxisinqlongname(zaxisid_dummy,longname_dummy)
integer(4),value::zaxisid_dummy
character(*,1),intent(inout)::longname_dummy
end
subroutine zaxisdefunits(zaxisid_dummy,units)
integer(4),value::zaxisid_dummy
character(*,1),intent(in),optional::units
end
subroutine zaxisinqunits(zaxisid_dummy,units_dummy)
integer(4),value::zaxisid_dummy
character(*,1),intent(inout)::units_dummy
end
subroutine zaxisinqstdname(zaxisid_dummy,stdname_dummy)
integer(4),value::zaxisid_dummy
character(*,1),intent(inout)::stdname_dummy
end
function zaxisinqlbounds(zaxisid_dummy,lbounds) result(f_result)
integer(4),value::zaxisid_dummy
real(8),intent(inout),optional,target::lbounds(1_8:*)
integer(4)::f_result
end
function zaxisinqubounds(zaxisid_dummy,ubounds) result(f_result)
integer(4),value::zaxisid_dummy
real(8),intent(inout),optional,target::ubounds(1_8:*)
integer(4)::f_result
end
function zaxisinqweights(zaxisid_dummy,weights) result(f_result)
integer(4),value::zaxisid_dummy
real(8),intent(inout),optional,target::weights(1_8:*)
integer(4)::f_result
end
function taxisnameptr(taxisid_dummy) result(f_result)
integer(4),value::taxisid_dummy
character(1_8,1),pointer::f_result(:)
end
function tunitnameptr(tunitid_dummy) result(f_result)
integer(4),value::tunitid_dummy
character(1_8,1),pointer::f_result(:)
end
function institutdef(center_dummy,subcenter_dummy,name_dummy,longname_dummy) result(f_result)
integer(4),value::center_dummy
integer(4),value::subcenter_dummy
character(*,1),intent(in)::name_dummy
character(*,1),intent(in)::longname_dummy
integer(4)::f_result
end
function institutinq(center_dummy,subcenter_dummy,name_dummy,longname_dummy) result(f_result)
integer(4),value::center_dummy
integer(4),value::subcenter_dummy
character(*,1),intent(in)::name_dummy
character(*,1),intent(in)::longname_dummy
integer(4)::f_result
end
function institutinqnameptr(instid_dummy) result(f_result)
integer(4),value::instid_dummy
character(1_8,1),pointer::f_result(:)
end
function institutinqlongnameptr(instid_dummy) result(f_result)
integer(4),value::instid_dummy
character(1_8,1),pointer::f_result(:)
end
function modeldef(instid_dummy,modelgribid_dummy,name_dummy) result(f_result)
integer(4),value::instid_dummy
integer(4),value::modelgribid_dummy
character(*,1),intent(in)::name_dummy
integer(4)::f_result
end
function modelinq(instid_dummy,modelgribid_dummy,name_dummy) result(f_result)
integer(4),value::instid_dummy
integer(4),value::modelgribid_dummy
character(*,1),intent(in)::name_dummy
integer(4)::f_result
end
function modelinqnameptr(modelid_dummy) result(f_result)
integer(4),value::modelid_dummy
character(1_8,1),pointer::f_result(:)
end
subroutine tablewrite(filename_dummy,tableid_dummy)
character(*,1),intent(in)::filename_dummy
integer(4),value::tableid_dummy
end
function tableread(tablefile_dummy) result(f_result)
character(*,1),intent(in)::tablefile_dummy
integer(4)::f_result
end
function tabledef(modelid_dummy,tablenum_dummy,tablename_dummy) result(f_result)
integer(4),value::modelid_dummy
integer(4),value::tablenum_dummy
character(*,1),intent(in)::tablename_dummy
integer(4)::f_result
end
function tableinqnameptr(tableid_dummy) result(f_result)
integer(4),value::tableid_dummy
character(1_8,1),pointer::f_result(:)
end
function tableinq(modelid_dummy,tablenum_dummy,tablename_dummy) result(f_result)
integer(4),value::modelid_dummy
integer(4),value::tablenum_dummy
character(*,1),intent(in)::tablename_dummy
integer(4)::f_result
end
subroutine tableinqentry(tableid_dummy,id_dummy,ltype_dummy,name_dummy,longname_dummy,units_dummy)
integer(4),value::tableid_dummy
integer(4),value::id_dummy
integer(4),value::ltype_dummy
character(*,1),intent(inout)::name_dummy
character(*,1),intent(inout)::longname_dummy
character(*,1),intent(inout)::units_dummy
end
function subtypeinqattribute(subtypeid_dummy,index_dummy,key_dummy,outvalue_dummy) result(f_result)
integer(4),value::subtypeid_dummy
integer(4),value::index_dummy
character(*,1),intent(in)::key_dummy
integer(4),intent(inout)::outvalue_dummy
integer(4)::f_result
end
end
